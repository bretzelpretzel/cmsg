﻿package  actions
{
	import flash.events.Event;

	public class VictoriousEvent extends Event 
	{
		// Event types.
		public static const EVENT_DEF:String = "Victorious";
 
		public function VictoriousEvent(type:String = VictoriousEvent.EVENT_DEF, bubbles:Boolean = false, cancelable:Boolean = false) 
		{
			super(type, bubbles, cancelable);
		}
 
		override public function clone():Event {
			// Return a new instance of this event with the same parameters.
			return new VictoriousEvent(type, bubbles, cancelable);
		}
	}
}
