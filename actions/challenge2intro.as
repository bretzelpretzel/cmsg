﻿package actions {

	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	import flash.net.URLRequest;
	import actions.cmsgmainloader;

	public class challenge2intro extends MovieClip {

		//		var cycles: int = new int();
		var Challenge2Score: Array = new Array();
		var mainStartButton: startButton = new startButton();
		var Countdown_title1: Number = new Number();

		public function challenge2intro() {
			// constructor code
			cmsgmainloader.cycles=0;
			Countdown_title1 = 2;
			//create timer
			var Count_title1: Timer = new Timer(1000, Countdown_title1);
			Count_title1.addEventListener(TimerEvent.TIMER, CountHandler_title1);
			Count_title1.start();
			//create start button
			mainStartButton.width = 450;
			mainStartButton.x = 95;
			mainStartButton.y = 650;
			mainStartButton.addEventListener(MouseEvent.CLICK, mainStartButtonClick);
		}

		public function mainStartButtonClick(e: Event) {
			remChild();
			//			advanceScene(e);
			dispatchEvent(new Event("sceneFF", true));

		}

		public function CountHandler_title1(event: TimerEvent): void {
			Countdown_title1--;
			if (Countdown_title1 == 0) {
				addChild(mainStartButton);
			}
		}

		public function remChild(): void {
			mainStartButton.removeEventListener(MouseEvent.CLICK, mainStartButtonClick);
			removeChild(mainStartButton);

			//			var scene_challenge2prestart:URLRequest = new URLRequest("challenge2prestart.swf");
			//			cmsg.changeScene(scene_challenge2prestart);
		}
	}
}