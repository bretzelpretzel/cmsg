﻿package actions {
	import actions.cmsgmainloader;
	import flash.display.MovieClip;
	import flash.ui.Multitouch;
	import flash.ui.MultitouchInputMode;
	//	import target;
	import Math;
	import flash.events.*;
	import flash.globalization.NumberFormatter;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	import flash.events.TouchEvent;
	import flash.display.DisplayObject;
	import flash.utils.Dictionary;
	import flash.text.TextField;
	import flash.text.TextFormat;

	public class challenge2main extends MovieClip {
		Multitouch.inputMode = MultitouchInputMode.TOUCH_POINT;
		var tempX1: Number = new Number();
		var tempY1: Number = new Number();
		var gtempX: Number = new Number();
		var gtempY: Number = new Number();
		var diameter1: int = new int();
		var gdiameter: int = new int();
		var curTouchPoints2: Dictionary = new Dictionary(); //a dictionary to store which objects are related to which touch point
		var fl_SecondsToCountDown1: Number = new Number();
		var bullseye1: target = new target();
		var goal: goalpost = new goalpost();
		var timer2: TextField = new TextField();
		var tf_timer: TextFormat = new TextFormat();
		public function challenge2main() {
			// constructor code

			cmsgmainloader.Challenge2Score[cmsgmainloader.cycles] = 0;
			diameter1 = 60;
			gdiameter = 100;

			//set up timer
			timer2.text = "20";
			timer2.x = 281.75;
			timer2.y = 0;
			timer2.height = 43.9;
			timer2.width = 40.2;
			tf_timer.size = 32;
			timer2.setTextFormat(tf_timer);
			addChild(timer2);
			/////////////////////////change this to adjust timer count!
			fl_SecondsToCountDown1 = 20;
			var fl_CountDownTimerInstance1: Timer = new Timer(1000, fl_SecondsToCountDown1);
			fl_CountDownTimerInstance1.addEventListener(TimerEvent.TIMER, fl_CountDownTimerHandler1);
			fl_CountDownTimerInstance1.start();

			//generate bullseyes
			bullseye1.width = diameter1;
			bullseye1.height = diameter1;
			randX1();
			randY1();

			//generate goalposts
			goal.width = gdiameter;
			goal.height = gdiameter;
			grandX();
			grandY();
			addChild(goal);
			//listen for touches on the target
			bullseye1.addEventListener(TouchEvent.TOUCH_BEGIN, touchStart2); //add both objects touch begin listener
			addChild(bullseye1);
			this.addEventListener(TouchEvent.TOUCH_MOVE, touchMove2);
		}

		public function fl_CountDownTimerHandler1(event: TimerEvent): void {
			trace(fl_SecondsToCountDown1 + " seconds");
			timer2.text = String(fl_SecondsToCountDown1);
			timer2.setTextFormat(tf_timer);
			fl_SecondsToCountDown1--;
			if (fl_SecondsToCountDown1 == 0) {
				stage.removeEventListener(TouchEvent.TOUCH_MOVE, touchMove2);
				bullseye1.removeEventListener(TouchEvent.TOUCH_BEGIN, touchStart2);
				removeChild(bullseye1);
				removeChild(goal);
				//increase cycle counter
				cmsgmainloader.cycles = cmsgmainloader.cycles + 1;
				if (cmsgmainloader.cycles == 4) {
					removeChild(timer2);
					dispatchEvent(new Event("sceneFF", true));
				} else {
					removeChild(timer2);
					dispatchEvent(new Event("sceneRev", true));
				}
			}
		}


		function touchStart2(e: TouchEvent): void {
			curTouchPoints2[e.touchPointID] = e.currentTarget; //store the current object in the dictionary
			e.currentTarget.addEventListener(TouchEvent.TOUCH_END, touchEnd2);
		}

		function touchMove2(e: TouchEvent): void {
			//move this object to the current touch position
			//find the object by referencing the dictionary (since e.currentTarget will be the stage, and e.target could be the child of what you really want OR the stage if touch 'leaves' the object)
			//if you get an error, don't worry. i think it's because i'm holding a "touch" down while off-screen. this shouldn't happen on a real device, only the emulator
			DisplayObject(curTouchPoints2[e.touchPointID]).x = e.stageX;
			DisplayObject(curTouchPoints2[e.touchPointID]).y = e.stageY;
			if (bullseye1.x + diameter1 / 2 > goal.x - gdiameter / 2 && bullseye1.x - diameter1 / 2 < goal.x + gdiameter / 2 && bullseye1.y + diameter1 / 2 > goal.y - gdiameter / 2 && bullseye1.y - diameter1 / 2 < goal.y + gdiameter / 2) {
				cmsgmainloader.Challenge2Score[cmsgmainloader.cycles]++;

				randX1();
				randY1();
				grandX();
				grandY();
				touchEnd2(e);
			}
		}

		function touchEnd2(e: TouchEvent): void {
			//remove the dictionary item now that the touch has ended
			//			if (bullseye1.x + diameter1 / 2 > goal.x - gdiameter / 2 && bullseye1.x - diameter1 / 2 < goal.x + gdiameter / 2 && bullseye1.y + diameter1 / 2 > goal.y - gdiameter / 2 && bullseye1.y - diameter1 / 2 < goal.y + gdiameter / 2) {
			//				trace("success");
			//				trace(cmsgmainloader.Challenge2Score);
			//			}
			delete curTouchPoints2[e.touchPointID];
//			e.currentTarget.removeEventListener(TouchEvent.TOUCH_END, touchEnd2);
			trace(cmsgmainloader.Challenge2Score);

		}

		public function randX1(): void {
			tempX1 = Math.random() * 640;
			randXCheck1();
		}

		public function randXCheck1(): void {
			if (tempX1 < diameter1 / 2) {
				randX1();

			} else if (tempX1 > 640 - diameter1 / 2) {
				randX1();
			} else
				bullseye1.x = tempX1;
		}
		public function randY1(): void {
			tempY1 = Math.random() * 960;
			randYCheck1();
		}

		public function randYCheck1(): void {
			if (tempY1 < 40 + diameter1 / 2) {
				randY1();
			} else if (tempY1 > 960 - diameter1 / 2) {
				randY1();
			} else {
				bullseye1.y = tempY1;
			}
		}


		public function grandX(): void {
			gtempX = Math.random() * 640;
			grandXCheck();
		}

		public function grandXCheck(): void {
			if (gtempX < gdiameter / 2) {
				grandX();
			} else if (gtempX > 640 - gdiameter / 2) {
				grandX();
			} else if (gtempX + gdiameter / 2 > bullseye1.x - diameter1 / 2 && gtempX - gdiameter / 2 < bullseye1.x + diameter1 / 2) {
				grandX();

			} else
				goal.x = gtempX;
		}
		public function grandY(): void {
			gtempY = Math.random() * 960;
			grandYCheck();
		}

		public function grandYCheck(): void {
			if (gtempY < 40 + gdiameter / 2) {
				grandY();
			} else if (gtempY > 960 - gdiameter / 2) {
				grandY();
			} else if (gtempY + gdiameter / 2 > bullseye1.y - diameter1 / 2 && gtempY - gdiameter / 2 < bullseye1.y + diameter1 / 2) {
				grandY();
			} else {
				goal.y = gtempY;
			}
		}
	}
}