﻿package actions {

	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	import flash.events.Event;
	import actions.cmsgmainloader;

	public class challenge2prestart extends MovieClip {
		var Countdown2: int = new int();
		//create left and write instruction symbols
		var leftinst: Left_Instruction = new Left_Instruction;
		var rightinst: Right_Instruction = new Right_Instruction;
		var Challenge2StartButton: startButton = new startButton();
		public function challenge2prestart() {
			// constructor code
			Countdown2 = 2;
			//make start button
			Challenge2StartButton.width = 450;
			Challenge2StartButton.x = 95;
			Challenge2StartButton.y = 500;
			Challenge2StartButton.addEventListener(MouseEvent.CLICK, Challenge2StartButtonClick);

			//check if left or right hand should be used
			if (cmsgmainloader.cycles == 0 || cmsgmainloader.cycles == 2) {
				addChild(leftinst);
				leftinst.y = 300;
			}
			if (cmsgmainloader.cycles == 1 || cmsgmainloader.cycles == 3) {
				addChild(rightinst);
				rightinst.y = 300;
			}
			var Count2: Timer = new Timer(1000, Countdown2);
			Count2.addEventListener(TimerEvent.TIMER, CountHandler2);
			Count2.start();
		}

		public function Challenge2StartButtonClick(e: Event) {
			remChild2();
			dispatchEvent(new Event("sceneFF", true));
		}

		public function CountHandler2(event: TimerEvent): void {
			Countdown2--;
			if (Countdown2 == 0) {
				addChild(Challenge2StartButton);
			}
		}


		public function remChild2(): void {
			removeChild(Challenge2StartButton);
			if (cmsgmainloader.cycles == 1 || cmsgmainloader.cycles == 3) {
				removeChild(rightinst);
			}
			if (cmsgmainloader.cycles == 0 || cmsgmainloader.cycles == 2) {
				removeChild(leftinst);
			}
		}
	}
}