﻿package actions {

	import flash.display.MovieClip;
	import flash.display.Loader;
	import flash.net.URLRequest;
	import flash.net.URLLoader;
	import flash.events.Event;



	public class cmsgmainloader extends MovieClip {
		//static variables
		public static var cycles: int = new int();
		public static var Challenge2Score: Array = new Array();
		public static var institution: String = new String();
		public static var uniqueID: int = new int();
		public static var age: Number = new Number();
		public static var sex: String = new String();
		public static var hand: String = new String();
		public static var mjoa: int = new int();
		public static var eat10: int = new int();
		public static var vhi10: int = new int();
		public static var ndi: int = new int();
		public static var sf36_physfunc: Number = new Number();
		public static var sf36_roleffuncphys: Number = new Number();
		public static var sf36_rolefuncemotion: Number = new Number();
		public static var sf36_energy: Number = new Number();
		public static var sf36_emotionalwellbeing: Number = new Number();
		public static var sf36_socialfunc: Number = new Number();
		public static var sf36_pain: Number = new Number();
		public static var sf36_generalhealth: Number = new Number();
		public static var sf36_healthchange: Number = new Number();
		public static var eq5d_descriptive: Number = new Number();
		public static var eq5d_vas: int = new int();
		public static var fmsRight1: int = new int();
		public static var fmsRight2: int = new int();
		public static var fmsLeft1: int = new int();
		public static var fmsLeft2: int = new int();


		//make urlrequest variables for each scene I plan to use
		//var scene_login: URLRequest = new URLRequest();
		//var scene_demographics: URLRequest = new URLRequest();
		//var scene_mjoa: URLRequest = new URLRequest();
		//var scene_eat10: URLRequest = new URLRequest();
		//var scene_vhi10: URLRequest = new URLRequest();
		//var scene_ndi: URLRequest = new URLRequest();
		//var scene_eq5d: URLRequest = new URLRequest();
		var scene_login: URLRequest = new URLRequest("login.swf");
		var scene_demographics: URLRequest = new URLRequest("demographics.swf");
		var scene_mjoa: URLRequest = new URLRequest("mjoa.swf");
		var scene_eat10: URLRequest = new URLRequest("eat10.swf");
		var scene_vhi10: URLRequest = new URLRequest("vhi10.swf");
		var scene_ndi: URLRequest = new URLRequest("ndi.swf");
		var scene_sf36: URLRequest = new URLRequest("sf36.swf");
		var scene_eq5dquestions: URLRequest = new URLRequest("eq5dquestions.swf");
		var scene_eq5dvas: URLRequest = new URLRequest("eq5dvas.swf");
		var scene_challenge2intro: URLRequest = new URLRequest("challenge2intro.swf");
		var scene_challenge2prestart: URLRequest = new URLRequest("challenge2prestart.swf");
		var scene_challenge2main: URLRequest = new URLRequest("challenge2main.swf");
		var scene_scorereview: URLRequest = new URLRequest("scorereview.swf");
		var sceneNumber: int = new int();
		var myLoader: Loader = new Loader(); // create a new instance of the Loader class
		var u: URLRequest = new URLRequest();

		public function cmsgmainloader() {

			// constructor code
			sceneNumber = 1;
			myLoader.addEventListener("sceneFF", advanceScene);
			myLoader.addEventListener("sceneRev", rewindScene);

			//pick the default swf to start with
			myLoader.load(scene_login); // load the SWF file
			addChild(myLoader);
		}
		public function advanceScene(e: Event): void {
			sceneNumber++;
			//return to start at finish of app cycle
			if (sceneNumber > 13) {
				sceneNumber = 1;
			}
			checkDirectory();
			//
		}

		public function rewindScene(e: Event): void {
			sceneNumber--;
			checkDirectory();
		}

		public function checkDirectory(): void {
			switch (sceneNumber) {
				case 2:
					u=scene_scorereview;
				
				/*	case 1:
					u = scene_login;
					break;
				case 2:
					u = scene_demographics;
					break;
				case 3:
					u = scene_mjoa;
					break;
				case 4:
					u = scene_eat10;
					break;
				case 5:
					u = scene_vhi10;
					break;
				case 6:
					u = scene_ndi;
					break;
				case 7:
					u=scene_sf36;
					break;
				case 8:
					u = scene_eq5dquestions;
					break;
				case 9:
					u = scene_eq5dvas;
					break;
				case 10:
					u = scene_challenge2intro;
					break;
				case 11:
					u = scene_challenge2prestart;
					break;
				case 12:
					u = scene_challenge2main;
					break;
				case 13:
					u = scene_scorereview;
					break; */
			}
			myLoader.unloadAndStop();
			myLoader.load(u);
		}
	}
}