﻿package actions {
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	import flash.events.Event;
	import flash.events.FocusEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.net.SharedObject;
	import fl.controls.RadioButton;
	import fl.controls.RadioButtonGroup;
	import flash.events.TimerEvent;
	import flash.utils.Timer;

	public class demographics extends MovieClip {

		var mainStartButton_res1: startButton = new startButton();
		var myFormat: TextFormat = new TextFormat();
		//create textbox variables
		var UniqueIDtextbox: TextField = new TextField();
		var YOBtextbox: TextField = new TextField();
		var MOBtextbox: TextField = new TextField();
		var DOBtextbox: TextField = new TextField();
		var tf_sex: TextFormat = new TextFormat();
		//create sex radiobuttons
		var rbsexgroup: RadioButtonGroup = new RadioButtonGroup("sex");
		var rbm: RadioButton = new RadioButton();
		var rbf: RadioButton = new RadioButton();
		//create handedness radiobuttons
		var rbhandgroup: RadioButtonGroup = new RadioButtonGroup("handedness");
		var rbr: RadioButton = new RadioButton();
		var rbl: RadioButton = new RadioButton();
		var todaysDate: Date = new Date();
		var birthDate: Date = new Date();

		public function demographics() {
			// constructor code
			////start button set up
			mainStartButton_res1.width = 450;
			mainStartButton_res1.x = 100;
			mainStartButton_res1.y = 720;
			mainStartButton_res1.addEventListener(MouseEvent.CLICK, mainStartButtonClick_res1);


			/////formatting textboxes
			//create formatting for textfields
			myFormat.size = 36;
			myFormat.align = TextFormatAlign.CENTER;
			UniqueIDtextbox.defaultTextFormat = myFormat;
			YOBtextbox.defaultTextFormat = myFormat;
			MOBtextbox.defaultTextFormat = myFormat;
			DOBtextbox.defaultTextFormat = myFormat;
			UniqueIDtextbox.border = false;
			UniqueIDtextbox.width = 231;
			UniqueIDtextbox.height = 43.9;
			UniqueIDtextbox.x = 320.9;
			UniqueIDtextbox.y = 101.85;
			UniqueIDtextbox.type = "input";
			UniqueIDtextbox.multiline = false;
			YOBtextbox.border = false;
			YOBtextbox.width = 231;
			YOBtextbox.height = 43.9;
			YOBtextbox.x = 320.9;
			YOBtextbox.y = 191.80;
			YOBtextbox.type = "input";
			YOBtextbox.multiline = false;
			MOBtextbox.border = false;
			MOBtextbox.width = 231;
			MOBtextbox.height = 43.9;
			MOBtextbox.x = 320.9;
			MOBtextbox.y = 276.95;
			MOBtextbox.type = "input";
			MOBtextbox.multiline = false;
			DOBtextbox.border = false;
			DOBtextbox.width = 231;
			DOBtextbox.height = 43.9;
			DOBtextbox.x = 320.9;
			DOBtextbox.y = 366.90;
			DOBtextbox.type = "input";
			DOBtextbox.multiline = false;
			//set default texts
			UniqueIDtextbox.text = "Unique ID #";
			YOBtextbox.text = "year of birth";
			MOBtextbox.text = "month of birth";
			DOBtextbox.text = "date of birth";
			//limitations on text
			UniqueIDtextbox.restrict = "0-9";
			YOBtextbox.maxChars = 4;
			YOBtextbox.restrict = "0-9";
			MOBtextbox.maxChars = 2;
			MOBtextbox.restrict = "0-9";
			DOBtextbox.maxChars = 2;
			DOBtextbox.restrict = "0-9";

			/////event listeners
			DOBtextbox.addEventListener(FocusEvent.FOCUS_IN, clearBox_DOB);
			DOBtextbox.addEventListener(FocusEvent.FOCUS_OUT, restoreBox_DOB);
			MOBtextbox.addEventListener(FocusEvent.FOCUS_IN, clearBox_MOB);
			MOBtextbox.addEventListener(FocusEvent.FOCUS_OUT, restoreBox_MOB);
			UniqueIDtextbox.addEventListener(FocusEvent.FOCUS_IN, clearBox_LI);
			UniqueIDtextbox.addEventListener(FocusEvent.FOCUS_OUT, restoreBox_LI);
			YOBtextbox.addEventListener(FocusEvent.FOCUS_IN, clearBox_YOB);
			YOBtextbox.addEventListener(FocusEvent.FOCUS_OUT, restoreBox_YOB);

			//stylize radiobuttons

			tf_sex.size = 34;
			tf_sex.align = TextFormatAlign.CENTER;
			rbm.x = 250;
			rbm.width = 250;
			rbm.y = 455;
			rbm.label = "Male"
			rbm.group = rbsexgroup;
			rbm.setStyle("textFormat", tf_sex);
			rbm.addEventListener(MouseEvent.CLICK, createStartButtonLinker);
			rbf.x = 440;
			rbf.width = 250;
			rbf.y = 455;
			rbf.label = "Female"
			rbf.group = rbsexgroup;
			rbf.setStyle("textFormat", tf_sex);
			rbf.addEventListener(MouseEvent.CLICK, createStartButtonLinker);
			rbr.x = 250;
			rbr.width = 250;
			rbr.y = 515;
			rbr.label = "Right"
			rbr.group = rbhandgroup;
			rbr.setStyle("textFormat", tf_sex);
			rbr.addEventListener(MouseEvent.CLICK, createStartButtonLinker);
			rbl.x = 440;
			rbl.width = 250;
			rbl.y = 515;
			rbl.label = "Left"
			rbl.group = rbhandgroup;
			rbl.setStyle("textFormat", tf_sex);
			rbl.addEventListener(MouseEvent.CLICK, createStartButtonLinker);
			//add children
			addChild(UniqueIDtextbox);
			addChild(YOBtextbox);
			addChild(MOBtextbox);
			addChild(DOBtextbox);
			addChild(rbm);
			addChild(rbf);
			addChild(rbr);
			addChild(rbl);

		}

		public function mainStartButtonClick_res1(e: Event) {
			saveDataObject();
			remChild_res1();
			dispatchEvent(new Event("sceneFF", true));
		}
		public function createStartButton(): void {
			if (UniqueIDtextbox.text != "" && UniqueIDtextbox.text != "Unique ID #") {
				if (YOBtextbox.text != "year of birth" && YOBtextbox.text != "invalid - yyyy" && YOBtextbox.text != "") {
					if (MOBtextbox.text != "month of birth" && MOBtextbox.text != "invalid - mm" && MOBtextbox.text != "") {
						if (DOBtextbox.text != "date of birth" && DOBtextbox.text != "invalid - dd" && DOBtextbox.text != "") {
							if (rbf.selected == true || rbm.selected == true) {
								if (rbr.selected == true || rbl.selected == true) {
									addChild(mainStartButton_res1);
								}
							}
						}
					}
				}
			};
		}
		public function saveDataObject() {
			birthDate.setUTCFullYear(Number(YOBtextbox.text), Number(MOBtextbox.text) - 1, Number(DOBtextbox.text));
			//	var age:Number = new Number();
			cmsgmainloader.age = (todaysDate.getTime() - birthDate.getTime()) / 1000 / 60 / 60 / 24 / 365;
			//	var uniqueID:int = new int();
			cmsgmainloader.uniqueID = int(UniqueIDtextbox.text);
			if (rbf.selected == true) {
				cmsgmainloader.sex = "Female";
			}
			if (rbm.selected == true) {
				cmsgmainloader.sex = "Male";
			}
			if (rbr.selected == true) {
				cmsgmainloader.hand = "Right";
			}
			if (rbl.selected == true) {
				cmsgmainloader.hand = "Left";
			}
		}
		public function remChild_res1(): void {
			mainStartButton_res1.removeEventListener(MouseEvent.CLICK, mainStartButtonClick_res1);
			removeChild(mainStartButton_res1);
			removeChild(UniqueIDtextbox);
			removeChild(YOBtextbox);
			removeChild(MOBtextbox);
			removeChild(DOBtextbox);
			removeChild(rbm);
			removeChild(rbf);
			removeChild(rbr);
			removeChild(rbl);
		}
		//default text erase and restore
		public function clearBox_LI(FocusEvent) {
			UniqueIDtextbox.text = ""; //To Clear the Text Box
			//remove start button if re-entering field
			if (this.contains(mainStartButton_res1)) {
				removeChild(mainStartButton_res1);
			}
		}
		public function restoreBox_LI(FocusEvent) {
			UniqueIDtextbox.text = UniqueIDtextbox.text.toUpperCase();
			if (UniqueIDtextbox.text == "") {
				UniqueIDtextbox.text = "Unique ID #";
			}
			createStartButton();
		}
		public function clearBox_YOB(FocusEvent) {
			YOBtextbox.text = ""; //To Clear the Text Box
			//remove start button if re-entering field
			if (this.contains(mainStartButton_res1)) {
				removeChild(mainStartButton_res1);
			}
		}
		public function restoreBox_YOB(FocusEvent) {
			if (YOBtextbox.text == "") {
				YOBtextbox.text = "year of birth";
			}
			if (YOBtextbox.text.length < 4) {
				YOBtextbox.text = "invalid - yyyy";
			}
			if (int(YOBtextbox.text) < 1900 || int(YOBtextbox.text) > 2050) {
				YOBtextbox.text = "invalid - yyyy";
			}
			createStartButton();
		}
		public function clearBox_MOB(FocusEvent) {
			MOBtextbox.text = ""; //To Clear the Text Box
			//remove start button if re-entering field
			if (this.contains(mainStartButton_res1)) {
				removeChild(mainStartButton_res1);
			}
		}
		public function restoreBox_MOB(FocusEvent) {
			if (MOBtextbox.text == "") {
				MOBtextbox.text = "month of birth";
			}
			if (MOBtextbox.text.length < 2) {
				MOBtextbox.text = "invalid - mm";
			}
			if (int(MOBtextbox.text) > 12) {
				MOBtextbox.text = "invalid - mm";
			}
			createStartButton();
		}
		public function clearBox_DOB(FocusEvent) {
			DOBtextbox.text = ""; //To Clear the Text Box
			//remove start button if re-entering field
			if (this.contains(mainStartButton_res1)) {
				removeChild(mainStartButton_res1);
			}
		}
		public function restoreBox_DOB(FocusEvent) {
			if (DOBtextbox.text == "") {
				DOBtextbox.text = "date of birth";
			}
			if (DOBtextbox.text.length < 2) {
				DOBtextbox.text = "invalid - dd";
			}
			if (int(DOBtextbox.text) > 31) {
				DOBtextbox.text = "invalid - dd";
			}
			createStartButton();
		}
		//link to other function
		public function createStartButtonLinker(e: Event) {
			createStartButton();
		}
	}
}