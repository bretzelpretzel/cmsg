﻿package actions {

	import flash.display.MovieClip;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import fl.controls.RadioButton;
	import fl.controls.RadioButtonGroup;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import actions.cmsgmainloader;



	public class eat10 extends MovieClip {
		//counting variables
		var i: int = new int();
		var j: int = new int();
		var k: int = new int();
		var l: int = new int();
		var m: int = new int();
		//other variables
		var eat10ScoreTotal: int = new int();
		//make text formats
		var tf_prompt: TextFormat = new TextFormat();
		var tf_next: TextFormat = new TextFormat();
		var tf_radios: TextFormat = new TextFormat();
		//make text box
		var eat10QuestionsTextBox: TextField = new TextField();
		//start at first question
		var eat10QuestionNumber: int = new int();
		//make question array
		var eat10QuestionsArray: Array = new Array();
		//establish the responses array
		var eat10ResponsesArray: Array = new Array();
		//create radiobuttongroups
		var rbGroupArray: Array = new Array();
		//create radiobutton array
		var rbArray: Array = new Array();
		//create variable to store scores temporarily
		var eat10ScoreArray: Array = new Array();
		//create dummy radiobutton
		var rbDummy:RadioButton = new RadioButton();

		public function eat10() {
			// constructor code
			//dummy rb set up
			rbDummy.visible=false;
			//
			eat10ScoreTotal = 0;
			tf_prompt.size = 36;
			tf_prompt.align = TextFormatAlign.CENTER;
			tf_radios.size = 14;
			tf_next.size = 32;
			eat10_NextButton.setStyle("textFormat", tf_next);
			eat10_BackButton.setStyle("textFormat", tf_next);
			//set up text box
			eat10QuestionsTextBox.wordWrap = true;
			eat10QuestionsTextBox.x = 0;
			eat10QuestionsTextBox.y = 62;
			eat10QuestionsTextBox.width = 640;
			eat10QuestionsTextBox.height = 140;
			eat10QuestionsTextBox.defaultTextFormat = tf_prompt;
			//start at first question
			eat10QuestionNumber = 0;
			//establish the questions array
			eat10QuestionsArray[0] = "My swallowing problem has caused me to lose weight.";
			eat10QuestionsArray[1] = "My swallowing problem interferes with my ability to go out for meals.";
			eat10QuestionsArray[2] = "Swallowing liquids takes extra effort.";
			eat10QuestionsArray[3] = "Swallowing solids takes extra effort.";
			eat10QuestionsArray[4] = "Swallowing pills takes extra effort.";
			eat10QuestionsArray[5] = "Swallowing is painful.";
			eat10QuestionsArray[6] = "The pleasure of eating is affected by my swallowing.";
			eat10QuestionsArray[7] = "When I swallow food sticks in my throat.";
			eat10QuestionsArray[8] = "I cough when I eat.";
			eat10QuestionsArray[9] = "Swallowing is stressful.";


			//establish the responses array
			eat10ResponsesArray[0] = ["0 - no problem", "1", "2", "3", "4 - severe problem"];
			eat10ResponsesArray[1] = ["0 - no problem", "1", "2", "3", "4 - severe problem"];
			eat10ResponsesArray[2] = ["0 - no problem", "1", "2", "3", "4 - severe problem"];
			eat10ResponsesArray[3] = ["0 - no problem", "1", "2", "3", "4 - severe problem"];
			eat10ResponsesArray[4] = ["0 - no problem", "1", "2", "3", "4 - severe problem"];
			eat10ResponsesArray[5] = ["0 - no problem", "1", "2", "3", "4 - severe problem"];
			eat10ResponsesArray[6] = ["0 - no problem", "1", "2", "3", "4 - severe problem"];
			eat10ResponsesArray[7] = ["0 - no problem", "1", "2", "3", "4 - severe problem"];
			eat10ResponsesArray[8] = ["0 - no problem", "1", "2", "3", "4 - severe problem"];
			eat10ResponsesArray[9] = ["0 - no problem", "1", "2", "3", "4 - severe problem"];

			////create radiobuttongroups
			//this section may be unnecessary
			for (j = 0; j < eat10QuestionsArray.length; j++) {
				var rbGroup: RadioButtonGroup = new RadioButtonGroup("options");
				rbGroupArray.push(rbGroup);
				resetRBs();
			}
			//set the starting values

			eat10QuestionsTextBox.text = eat10QuestionsArray[eat10QuestionNumber];
			addChild(eat10QuestionsTextBox);
			//advance the questions based on the button-presses
			eat10_NextButton.addEventListener(MouseEvent.CLICK, eat10_NextClick);
			eat10_BackButton.addEventListener(MouseEvent.CLICK, eat10_BackClick);

		}

		public function resetRBs(): void {
			for (i = 0; i < eat10ResponsesArray[eat10QuestionNumber].length; i++) {
				var rb: RadioButton = new RadioButton();

				rbArray.push(rb);
				rbArray[i].label = eat10ResponsesArray[eat10QuestionNumber][i];
				rbArray[i].x = 13;
				rbArray[i].y = 230 + 65 * i;
				rbArray[i].setStyle("textFormat", tf_radios);
				rbArray[i].width = 600;
				rbArray[i].height = 65;
				rbArray[i].value = 1;
				//	rbArray[i].textField.autoSize=TextFieldAutoSize.CENTER;
				//this following line is probably unnecessary
				rbArray[i].group = rbGroupArray[eat10QuestionNumber];
				//since AS3 is stupid and you can't set radiobutton.selected to false, i will create a dummy radiobutton to select during transitions
				rbDummy.group = rbGroupArray[eat10QuestionNumber];
				rbDummy.selected = true;
				addChild(rbArray[i]);
			}
		}

		public function eat10_NextClick(e: Event) {
			//only advance if somethin is selected
			//store the data
			for (k = 0; k < rbArray.length; k++) {
				if (rbArray[k].selected == true) {
					eat10ScoreArray[eat10QuestionNumber] = k;
					//makes sure something is selected
					NextClickProceed();
				}

			}
		}

		public function NextClickProceed(): void {
			//reset value of rbArray while removing its children
			for (l = rbArray.length - 1; l >= 0; l--) {
				if (contains(rbArray[l])) {
					removeChild(rbArray[l]);
					rbArray.pop();
				}
			}

			//transition to new section when finished
			if (eat10QuestionNumber < eat10QuestionsArray.length - 1) {
				//generate new responses
				eat10QuestionNumber++;
				eat10QuestionsTextBox.text = eat10QuestionsArray[eat10QuestionNumber];
				resetRBs();
			} else {
				trace("test complete");
				//remove all children
				removeChild(eat10QuestionsTextBox);
				removeChild(eat10_NextButton);
				removeChild(eat10_BackButton);

				for (i = 0; i < eat10ScoreArray.length; i++) {
					eat10ScoreTotal += eat10ScoreArray[i];
				}
				cmsgmainloader.eat10 = eat10ScoreTotal;
				dispatchEvent(new Event("sceneFF", true));
			}
		}

		public function eat10_BackClick(e: Event) {
			//reset value of rbArray while removing its children
			for (m = rbArray.length - 1; m >= 0; m--) {
				if (contains(rbArray[m])) {
					removeChild(rbArray[m]);
					rbArray.pop();
				}
			}
			//generate new responses
			eat10QuestionNumber--;
			//transition to prior section when pushing back button
			if (eat10QuestionNumber < 0) {
				removeChild(eat10QuestionsTextBox);
				dispatchEvent(new Event("sceneRev", true));
			} else {
				trace("this is question " + eat10QuestionNumber + " and there are " + eat10QuestionsArray.length + " questions");
				eat10QuestionsTextBox.text = eat10QuestionsArray[eat10QuestionNumber];
				resetRBs();
			}

		}
	}

}