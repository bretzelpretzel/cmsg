﻿package actions {
	

	import flash.display.MovieClip;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import fl.controls.RadioButton;
	import fl.controls.RadioButtonGroup;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import actions.cmsgmainloader;
	import qalydata.eq5dq_calculator;




	public class eq5dq extends MovieClip {
		//counting variables
		var i: int = new int();
		var j: int = new int();
		var k: int = new int();
		var l: int = new int();
		var m: int = new int();
		//other variables
		var eq5dqScoreTotal: int = new int();
		//make text formats
		var tf_prompt: TextFormat = new TextFormat();
		var tf_next: TextFormat = new TextFormat();
		var tf_radios: TextFormat = new TextFormat();
		//make text box
		var eq5dqQuestionsTextBox: TextField = new TextField();
		//start at first question
		var eq5dqQuestionNumber: int = new int();
		//make question array
		var eq5dqQuestionsArray: Array = new Array();
		//establish the responses array
		var eq5dqResponsesArray: Array = new Array();
		//create radiobuttongroups
		var rbGroupArray: Array = new Array();
		//create radiobutton array
		var rbArray: Array = new Array();
		//create variable to store scores temporarily
		var eq5dqScoreArray: Array = new Array();
		//dummy radiobutton
		var rbDummy:RadioButton = new RadioButton();
		//eq5d calculator instance
		var calculator:eq5dq_calculator = new eq5dq_calculator();

		public function eq5dq() {
			
			// constructor code
			//make dummy rb
			rbDummy.visible=false;
			//
			eq5dqScoreTotal = 0;
			tf_prompt.size = 36;
			tf_prompt.align = TextFormatAlign.CENTER;
			tf_radios.size = 14;
			tf_next.size = 32;
			eq5dq_NextButton.setStyle("textFormat", tf_next);
			eq5dq_BackButton.setStyle("textFormat", tf_next);
			//set up text box
			eq5dqQuestionsTextBox.wordWrap = true;
			eq5dqQuestionsTextBox.x = 0;
			eq5dqQuestionsTextBox.y = 70;
			eq5dqQuestionsTextBox.width = 640;
			eq5dqQuestionsTextBox.height = 140;
			eq5dqQuestionsTextBox.defaultTextFormat = tf_prompt;
			//start at first question
			eq5dqQuestionNumber = 0;
			//establish the questions array
			eq5dqQuestionsArray[0] = "Mobility";
			eq5dqQuestionsArray[1] = "Self-Care";
			eq5dqQuestionsArray[2] = "Usual Activities (e.g., work, study, housework, family or leisure actvities)";
			eq5dqQuestionsArray[3] = "Pain / Discomfort";
			eq5dqQuestionsArray[4] = "Anxiety / Depression";
			//establish the responses array
			eq5dqResponsesArray[0] = ["I have no problems in walking about","I have slight problems in walking about","I have moderate problems in walking about", "I have severe problems in walking about", "I am unable to walk about"];
			eq5dqResponsesArray[1] = ["I have no problems washing or dressing myself","I have slight problems washing or dressing myself","I have moderate problems washing or dressing myself","I have severe problems washing or dressing myself","I am unable to wash or dress myself"];
			eq5dqResponsesArray[2] = ["I have no problems in doing my usual activities","I have slight problems doing my usual actvities","I have moderate problems doing my usual activities","I have severe problems doing my usual activities","I am unable to do my usual activities"];
			eq5dqResponsesArray[3] = ["I have no pain or discomfort","I have slight pain or discomfort","I have moderate pain or discomfort","I have severe pain or discomfort","I have extreme pain or discomfort"];
			eq5dqResponsesArray[4] = ["I am not anxious or depressed","I am slightly anxious or depressed","I am moderate anxious or depressed","I am severely anxious or depressed","I am extremely anxious or depressed"];
			////create radiobuttongroups
			//this section may be unnecessary
			for (j = 0; j < eq5dqQuestionsArray.length; j++) {
				var rbGroup: RadioButtonGroup = new RadioButtonGroup("options");
				rbGroupArray.push(rbGroup);
				resetRBs();
			}
			//set the starting values

			eq5dqQuestionsTextBox.text = eq5dqQuestionsArray[eq5dqQuestionNumber];
			addChild(eq5dqQuestionsTextBox);
			//advance the questions based on the button-presses
			eq5dq_NextButton.addEventListener(MouseEvent.CLICK, eq5dq_NextClick);
			eq5dq_BackButton.addEventListener(MouseEvent.CLICK, eq5dq_BackClick);

		}

		public function resetRBs(): void {
			for (i = 0; i < eq5dqResponsesArray[eq5dqQuestionNumber].length; i++) {
				var rb: RadioButton = new RadioButton();

				rbArray.push(rb);
				rbArray[i].label = eq5dqResponsesArray[eq5dqQuestionNumber][i];
				rbArray[i].x = 13;
				rbArray[i].y = 230 + 65 * i;
				rbArray[i].setStyle("textFormat", tf_radios);
				rbArray[i].width = 600;
				rbArray[i].height = 65;
				rbArray[i].value = 1;
				//	rbArray[i].textField.autoSize=TextFieldAutoSize.CENTER;
				//this following line is probably unnecessary
				rbArray[i].group = rbGroupArray[eq5dqQuestionNumber];
				//since AS3 is stupid and you can't set radiobutton.selected to false, i will create a dummy radiobutton to select during transitions
				rbDummy.group = rbGroupArray[eq5dqQuestionNumber];
				rbDummy.selected = true;
				addChild(rbArray[i]);
			}
		}

		public function eq5dq_NextClick(e: Event) {
			//only advance if somethin is selected
			//store the data
			for (k = 0; k < rbArray.length; k++) {
				if (rbArray[k].selected == true) {
					//putting a +1 here because the first option is a 1, by convention with eq5d, not a 0
					eq5dqScoreArray[eq5dqQuestionNumber] = k+1;
					//makes sure something is selected
					NextClickProceed();
				}

			}
		}

		public function NextClickProceed(): void {
			//reset value of rbArray while removing its children
			for (l = rbArray.length - 1; l >= 0; l--) {
				if (contains(rbArray[l])) {
					removeChild(rbArray[l]);
					rbArray.pop();
				}

			}

			//transition to new section when finished
			if (eq5dqQuestionNumber < eq5dqQuestionsArray.length - 1) {
				//generate new responses
				eq5dqQuestionNumber++;
				eq5dqQuestionsTextBox.text = eq5dqQuestionsArray[eq5dqQuestionNumber];
				resetRBs();
			} else {
				trace("test complete");
				//remove all children
				removeChild(eq5dqQuestionsTextBox);
				removeChild(eq5dq_NextButton);
				removeChild(eq5dq_BackButton);

				//this is to generate the "string" of scores that the EQ-5D calculator will then interpret to spit out a QALY
				eq5dqScoreTotal = int(eq5dqScoreArray[0])*10000 + int(eq5dqScoreArray[1])*1000 + int(eq5dqScoreArray[2])*100 + int(eq5dqScoreArray[3])*10 + int(eq5dqScoreArray[4]);
				trace(eq5dqScoreTotal);
				//now let's save that to my static public variable
				calculator.calculateQALYs(eq5dqScoreTotal);
				dispatchEvent(new Event("sceneFF", true));
			}
		}

		public function eq5dq_BackClick(e: Event) {
			//reset value of rbArray while removing its children
			for (m = rbArray.length - 1; m >= 0; m--) {
				if (contains(rbArray[m])) {
					removeChild(rbArray[m]);
					rbArray.pop();
				}
			}
			//generate new responses
			eq5dqQuestionNumber--;
			//transition to prior section when pushing back button
			if (eq5dqQuestionNumber < 0) {
				removeChild(eq5dqQuestionsTextBox);
				dispatchEvent(new Event("sceneRev", true));
			} else {
				trace("this is question " + eq5dqQuestionNumber + " and there are " + eq5dqQuestionsArray.length + " questions");
				eq5dqQuestionsTextBox.text = eq5dqQuestionsArray[eq5dqQuestionNumber];
				resetRBs();
			}

		}
	}

}