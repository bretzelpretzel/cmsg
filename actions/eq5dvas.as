﻿package actions {

	import flash.display.MovieClip;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import actions.cmsgmainloader;
	import fl.controls.Slider;



	public class eq5dvas extends MovieClip {

		public function eq5dvas() {
			//advance the questions based on the button-presses
			eq5dvas_NextButton.addEventListener(MouseEvent.CLICK, eq5dvas_NextClick);
			eq5dvas_BackButton.addEventListener(MouseEvent.CLICK, eq5dvas_BackClick);
			//place slider
			eq5dvasTextBox.addEventListener(Event.ENTER_FRAME, updateScore);
/*			vasSlider.addEventListener(MouseEvent.MOUSE_UP, updateScore);
			vasSlider.addEventListener(MouseEvent.MOUSE_DOWN, updateScore);
			vasSlider.addEventListener(MouseEvent.RELEASE_OUTSIDE, updateScore);
			vasSlider.addEventListener(MouseEvent.MOUSE_OUT, updateScore);
			vasSlider.addEventListener(MouseEvent.MOUSE_MOVE, updateScore);
			vasSlider.addEventListener(MouseEvent.CLICK, updateScore);*/

		}

		public function updateScore(e:Event){
			eq5dvasTextBox.text=vasSlider.value.toString();
		}
		public function eq5dvas_NextClick(e: Event) {
			cmsgmainloader.eq5d_vas=vasSlider.value;
			dispatchEvent(new Event("sceneFF", true));
		}

		
		public function eq5dvas_BackClick(e: Event) {
				dispatchEvent(new Event("sceneRev", true));
		}
	}
}