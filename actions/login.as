﻿package actions {

	/*
    always extend a class using movieclip instead of sprite when using flash.
    */

	import flash.display.MovieClip;
	import flash.events.*;
	import flash.net.*;
	import flash.text.*;
	import flash.net.SharedObject;
	import actions.cmsgmainloader;

	/*
    create our class
    */

	public class login extends MovieClip {
		//define all public scoring variables
		var institution: String = new String();
		var uniqueID: int = new int();
		var age: Number = new Number();
		var sex: String = new String();
		var hand: String = new String();


		public function login(): void {
			//set up appearances
			var tf_submitbutton: TextFormat = new TextFormat();
			tf_submitbutton.size = 36;
			tf_submitbutton.align = TextFormatAlign.CENTER;
			submit_button.setStyle("textFormat", tf_submitbutton);
			//change appearance of input textboxes
			username.border = true;
			password.border = true;

			/*
            buttonMode gives the submit button a rollover
            */


			submit_button.buttonMode = true;

			/*
            what this says is that when our button is pressed, the checkLogin function will run
            */

			submit_button.addEventListener(MouseEvent.MOUSE_DOWN, checkLogin);

			/*
            set the initial textfield values
            */

			username.text = "";
			password.text = "";

		}
		public function checkLogin(e: MouseEvent): void {

			/*
    check fields before sending request to php
    */

			if (username.text == "" || password.text == "") {

				/*
        if username or password fields are empty set error messages
        */

				if (username.text == "") {

					username.text = "username";

				}

				if (password.text == "") {

					password.text = "password";

				}

			} else {

				/*
        init function to process login
        */

				processLogin();

			}

		}
		/*
function to process our login
*/

		public function processLogin(): void {

			/*
    variables that we send to the php file
    */

			var phpVars: URLVariables = new URLVariables();

			/*
    we create a URLRequest  variable. This gets the php file path.
    */
			/*THIS PART WILL NEED TO CHANGE BEFORE TRANSFERRING TO REAL SERVER!! */

		//	var phpFileRequest: URLRequest = new URLRequest("http://localhost/CMSG/php/controlpanel.php");
		//changing for remote host
			var phpFileRequest: URLRequest = new URLRequest("../php_remote/controlpanel.php");
			/*
    this allows us to use the post function in php
    */

			phpFileRequest.method = URLRequestMethod.POST;

			/*
    attach the php variables to the URLRequest
    */

			phpFileRequest.data = phpVars;

			/*
    create a new loader to load and send our urlrequest
    */

			var phpLoader: URLLoader = new URLLoader();
			phpLoader.dataFormat = URLLoaderDataFormat.VARIABLES;
			phpLoader.addEventListener(Event.COMPLETE, showResult);

			/*
    now lets create the variables to send to the php file
    */

			phpVars.systemCall = "checkLogin";
			phpVars.username = username.text;
			phpVars.password = password.text;

			/*
    this will start the communication between flash and php
    */

			phpLoader.load(phpFileRequest);
			phpLoader.addEventListener(Event.COMPLETE, showResult);

		}
		/*
function to show the result of the login
*/

		public function showResult(event: Event): void {

			/*
     
    this autosizes the text field
     
    ***** You will need to import flash's text classes. You can do this by adding: 
	
    import flash.text.*;
     
    ...to your list of import statements 
     
    */

			result_text.autoSize = TextFieldAutoSize.CENTER;

			/*
    this gets the output and displays it in the result text field
    */

			result_text.text = event.target.data.systemResult;
			if (result_text.text == "success") {
				cmsgmainloader.institution = username.text;
				dispatchEvent(new Event("sceneFF", true));
			}

		}
	}

}