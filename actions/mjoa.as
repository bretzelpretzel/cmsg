﻿package actions {

	import flash.display.MovieClip;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import fl.controls.RadioButton;
	import fl.controls.RadioButtonGroup;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import actions.cmsgmainloader;



	public class mjoa extends MovieClip {
		//counting variables
		var i: int = new int();
		var j: int = new int();
		var k: int = new int();
		var l: int = new int();
		var m: int = new int();
		//other variables
		var mJOAScoreTotal: int = new int();
		//make text formats
		var tf_prompt: TextFormat = new TextFormat();
		var tf_next: TextFormat = new TextFormat();
		var tf_radios: TextFormat = new TextFormat();
		//make text box
		var mJOAQuestionsTextBox: TextField = new TextField();
		//start at first question
		var mJOAQuestionNumber: int = new int();
		//make question array
		var mJOAQuestionsArray: Array = new Array();
		//establish the responses array
		var mJOAResponsesArray: Array = new Array();
		//create radiobuttongroups
		var rbGroupArray: Array = new Array();
		//create radiobutton array
		var rbArray: Array = new Array();
		//create variable to store scores temporarily
		var mJOAScoreArray: Array = new Array();
		//dummy radiobutton
		var rbDummy:RadioButton = new RadioButton();

		public function mjoa() {
			
			// constructor code
			//make dummy rb
			rbDummy.visible=false;
			//
			mJOAScoreTotal = 0;
			tf_prompt.size = 36;
			tf_prompt.align = TextFormatAlign.CENTER;
			tf_radios.size = 14;
			tf_next.size = 32;
			mJOA_NextButton.setStyle("textFormat", tf_next);
			mJOA_BackButton.setStyle("textFormat", tf_next);
			//set up text box
			mJOAQuestionsTextBox.wordWrap = true;
			mJOAQuestionsTextBox.x = 0;
			mJOAQuestionsTextBox.y = 62;
			mJOAQuestionsTextBox.width = 640;
			mJOAQuestionsTextBox.height = 140;
			mJOAQuestionsTextBox.defaultTextFormat = tf_prompt;
			//start at first question
			mJOAQuestionNumber = 0;
			//establish the questions array
			mJOAQuestionsArray[0] = "Which best describes the use of your upper extremities at this time?";
			mJOAQuestionsArray[1] = "Which best describes the use of your lower extremities at this time?";
			mJOAQuestionsArray[2] = "Which best describes the sensation to your upper extremities?";
			mJOAQuestionsArray[3] = "Which best describes your urination at this time?";
			//establish the responses array
			mJOAResponsesArray[0] = ["Inability to move hands", "Inability to eat with spoon, but able to move hands", "Inability to button shirt, but able to eat with a spoon", "Able to button shirt with great difficulty", "Able to button shirt with slight difficulty", "No dysfunction"];
			mJOAResponsesArray[1] = ["Complete loss of motor and sensory function", "Sensory preservation without ability to move legs", "Able to move legs, but unable to walk", "Able to walk on a flat floor with a walking aid", "Able to walk up and down stairs with a hand rail", "Moderate to significant instability but able to walk up and down stairs without a hand rail", "Mild lack of stability but walks with smooth unaided gait", "No dysfunction"];
			mJOAResponsesArray[2] = ["Complete loss of hand sensation", "Severe sensory loss or pain", "Mild sensory loss", "No sensory loss"];
			mJOAResponsesArray[3] = ["Inability to urinate voluntarily", "Marked difficulty with urination", "Mild to moderate difficulty with urination", "Normal urination"];
			////create radiobuttongroups
			//this section may be unnecessary
			for (j = 0; j < mJOAQuestionsArray.length; j++) {
				var rbGroup: RadioButtonGroup = new RadioButtonGroup("options");
				rbGroupArray.push(rbGroup);
				resetRBs();
			}
			//set the starting values

			mJOAQuestionsTextBox.text = mJOAQuestionsArray[mJOAQuestionNumber];
			addChild(mJOAQuestionsTextBox);
			//advance the questions based on the button-presses
			mJOA_NextButton.addEventListener(MouseEvent.CLICK, mJOA_NextClick);
			mJOA_BackButton.addEventListener(MouseEvent.CLICK, mJOA_BackClick);

		}

		public function resetRBs(): void {
			for (i = 0; i < mJOAResponsesArray[mJOAQuestionNumber].length; i++) {
				var rb: RadioButton = new RadioButton();

				rbArray.push(rb);
				rbArray[i].label = mJOAResponsesArray[mJOAQuestionNumber][i];
				rbArray[i].x = 13;
				rbArray[i].y = 230 + 65 * i;
				rbArray[i].setStyle("textFormat", tf_radios);
				rbArray[i].width = 600;
				rbArray[i].height = 65;
				rbArray[i].value = 1;
				//	rbArray[i].textField.autoSize=TextFieldAutoSize.CENTER;
				//this following line is probably unnecessary
				rbArray[i].group = rbGroupArray[mJOAQuestionNumber];
				//since AS3 is stupid and you can't set radiobutton.selected to false, i will create a dummy radiobutton to select during transitions
				rbDummy.group = rbGroupArray[mJOAQuestionNumber];
				rbDummy.selected = true;
				addChild(rbArray[i]);
			}
		}

		public function mJOA_NextClick(e: Event) {
			//only advance if somethin is selected
			//store the data
			for (k = 0; k < rbArray.length; k++) {
				if (rbArray[k].selected == true) {
					mJOAScoreArray[mJOAQuestionNumber] = k;
					//makes sure something is selected
					NextClickProceed();
				}

			}
		}

		public function NextClickProceed(): void {
			//reset value of rbArray while removing its children
			for (l = rbArray.length - 1; l >= 0; l--) {
				if (contains(rbArray[l])) {
					removeChild(rbArray[l]);
					rbArray.pop();
				}

			}

			//transition to new section when finished
			if (mJOAQuestionNumber < mJOAQuestionsArray.length - 1) {
				//generate new responses
				mJOAQuestionNumber++;
				mJOAQuestionsTextBox.text = mJOAQuestionsArray[mJOAQuestionNumber];
				resetRBs();
			} else {
				trace("test complete");
				//remove all children
				removeChild(mJOAQuestionsTextBox);
				removeChild(mJOA_NextButton);
				removeChild(mJOA_BackButton);

				for (i = 0; i < mJOAScoreArray.length; i++) {
					mJOAScoreTotal += mJOAScoreArray[i];
				}
				cmsgmainloader.mjoa = mJOAScoreTotal;
				dispatchEvent(new Event("sceneFF", true));
			}
		}

		public function mJOA_BackClick(e: Event) {
			//reset value of rbArray while removing its children
			for (m = rbArray.length - 1; m >= 0; m--) {
				if (contains(rbArray[m])) {
					removeChild(rbArray[m]);
					rbArray.pop();
				}
			}
			//generate new responses
			mJOAQuestionNumber--;
			//transition to prior section when pushing back button
			if (mJOAQuestionNumber < 0) {
				removeChild(mJOAQuestionsTextBox);
				dispatchEvent(new Event("sceneRev", true));
			} else {
				trace("this is question " + mJOAQuestionNumber + " and there are " + mJOAQuestionsArray.length + " questions");
				mJOAQuestionsTextBox.text = mJOAQuestionsArray[mJOAQuestionNumber];
				resetRBs();
			}

		}
	}

}