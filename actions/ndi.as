﻿package actions {

	import flash.display.MovieClip;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.text.TextFieldAutoSize;
	import fl.controls.RadioButton;
	import fl.controls.RadioButtonGroup;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import actions.cmsgmainloader;

	public class ndi extends MovieClip {
		//counting variables
		var i: int = new int();
		var j: int = new int();
		var k: int = new int();
		var l: int = new int();
		var m: int = new int();
		//other variables
		var ndiScoreTotal: int = new int();
		//make text formats
		var tf_prompt: TextFormat = new TextFormat();
		var tf_next: TextFormat = new TextFormat();
		var tf_radios: TextFormat = new TextFormat();
		//make text box
		var ndiQuestionsTextBox: TextField = new TextField();
		//start at first question
		var ndiQuestionNumber: int = new int();
		//make question array
		var ndiQuestionsArray: Array = new Array();
		//establish the responses array
		var ndiResponsesArray: Array = new Array();
		//create radiobuttongroups
		var rbGroupArray: Array = new Array();
		//create radiobutton array
		var rbArray: Array = new Array();
		//create variable to store scores temporarily
		var ndiScoreArray: Array = new Array();
		//dummy radiobutton
		var rbDummy: RadioButton = new RadioButton();


		public function ndi() {
			// constructor code
			//make dummy radiobutton
			rbDummy.visible=false;
			//
			ndiScoreTotal = 0;
			tf_prompt.size = 36;
			tf_prompt.align = TextFormatAlign.CENTER;
			tf_radios.size = 14;
			tf_next.size = 32;
			ndi_NextButton.setStyle("textFormat", tf_next);
			ndi_BackButton.setStyle("textFormat", tf_next);
			//set up text box
			ndiQuestionsTextBox.wordWrap = true;
			ndiQuestionsTextBox.x = 0;
			ndiQuestionsTextBox.y = 62;
			ndiQuestionsTextBox.width = 640;
			ndiQuestionsTextBox.height = 140;
			ndiQuestionsTextBox.defaultTextFormat = tf_prompt;
			//start at first question
			ndiQuestionNumber = 0;
			//establish the questions array
			ndiQuestionsArray[0] = "Pain Intensity";
			ndiQuestionsArray[1] = "Personal Care (Washing, Dressing, etc.)";
			ndiQuestionsArray[2] = "Lifting";
			ndiQuestionsArray[3] = "Reading";
			ndiQuestionsArray[4] = "Headaches";
			ndiQuestionsArray[5] = "Concentration";
			ndiQuestionsArray[6] = "Work";
			ndiQuestionsArray[7] = "Driving";
			ndiQuestionsArray[8] = "Sleeping";
			ndiQuestionsArray[9] = "Recreation";


			//establish the responses array
			ndiResponsesArray[0] = ["I have no pain at the moment", "The pain is very mild at the moment", "The pain is moderate at the moment", "The pain is fairly severe at the moment", "The pain is very severe at the moment", "The pain is the worst imaginable at the moment"];
			ndiResponsesArray[1] = ["I can look after myself normally without causing extra pain", "I can look after myself normally but it causes extra pain", "It is painful to look after myself and I am slow and careful", "I need some help but can manage most of my personal care", "I need help every day in most aspects of self care", "I do not get dressed, I wash with difficulty and stay in bed"];
			ndiResponsesArray[2] = ["I can lift heavy weights without extra pain", "I can lift heavy weights but it gives me extra pain", "Pain prevents me lifting heavy weights off the floor, but I can manage if they are conveniently placed, for example on a table", "Pain prevents me from lifting heavy weights but I can manage light to medium weights if they are conveniently positioned", "I can only lift very light weights", "I cannot lift or carry anything"];
			ndiResponsesArray[3] = ["I can read as much as I want with no pain in my neck", "I can read as much as I want to with slight pain in my neck", "I can read as much as I want with moderate pain in my neck", "I can't read as much as I want because of moderate pain in my neck", "I can hardly read at all because of severe pain in my neck", "I cannot read at all"];
			ndiResponsesArray[4] = ["I have no headaches at all", "I have slight headaches, which come infrequently", "I have moderate headaches, which come infrequently", "I have moderate headaches, which come frequently", "I have severe headaches, which come frequently", "I have headaches almost all the time"];
			ndiResponsesArray[5] = ["I can concentrate fully when I want to with no difficulty", "I can concentrate fully when I want to with slight difficulty", "I have a fair degree of difficulty in concentrating when I want to", "I have a lot of difficulty in concentrating when I want to", "I have a great deal of difficulty in concentrating when I want to", "I cannot concentrate at all"];
			ndiResponsesArray[6] = ["I can do as much work as I want to", "I can only do my usual work, but no more", "I can do most of my usual work, but no more", "I cannot do my usual work", "I can hardly do any work at all", "I can't do any work at all"];
			ndiResponsesArray[7] = ["I can drive my car without any neck pain", "I can drive my car as long as I want with slight pain in my neck", "I can drive my car as long as I want with moderate pain in my neck", "I can't drive my car as long as I want because of moderate pain in my neck", "I can hardly drive at all because of severe pain in my neck", "I can't drive my car at all"];
			ndiResponsesArray[8] = ["I have no trouble sleeping", "My sleep is slightly disturbed (less than 1 hr sleepless)", "My sleep is mildly disturbed (1-2 hrs sleepless)", "My sleep is moderately disturbed (2-3 hrs sleepless)", "My sleep is greatly disturbed (3-5 hrs sleepless)", "My sleep is completely disturbed (5-7 hrs sleepless)"];
			ndiResponsesArray[9] = ["I am able to engage in all my recreation actvities with no neck pain at all", "I am able to engage in all my recreation activities, with some pain in my neck", "I am able to engage in most, but not all of my usual recreation activities because of pain in my neck", "I am able to engage in a few of my usual recreation activities because of pain in my neck", "I can hardly do any recreation activities because of pain in my neck", "I can't do any recreation activities at all"];

			////create radiobuttongroups
			//this section may be unnecessary
			for (j = 0; j < ndiQuestionsArray.length; j++) {
				var rbGroup: RadioButtonGroup = new RadioButtonGroup(ndiQuestionsArray[j]);
				rbGroupArray.push(rbGroup);
				resetRBs();
			}
			//set the starting values

			ndiQuestionsTextBox.text = ndiQuestionsArray[ndiQuestionNumber];
			addChild(ndiQuestionsTextBox);
			//advance the questions based on the button-presses
			ndi_NextButton.addEventListener(MouseEvent.CLICK, ndi_NextClick);
			ndi_BackButton.addEventListener(MouseEvent.CLICK, ndi_BackClick);

		}

		public function resetRBs(): void {
			trace("now I am using rb Group " + ndiQuestionNumber + "which is a " + rbGroupArray[ndiQuestionNumber]);

			for (i = 0; i < ndiResponsesArray[ndiQuestionNumber].length; i++) {
				var rb: RadioButton = new RadioButton();
				rbArray.push(rb);
				rbArray[i].label = ndiResponsesArray[ndiQuestionNumber][i];
				rbArray[i].x = 13;
				rbArray[i].y = 230 + 65 * i;
				rbArray[i].setStyle("textFormat", tf_radios);
				rbArray[i].width = 640;
				rbArray[i].textField.width = 600;
				rbArray[i].height = 65;
				rbArray[i].value = 1;
				//////////
				rbArray[i].textField.multiline = true;
				rbArray[i].textField.wordWrap = true;
				rbArray[i].textField.autoSize = TextFieldAutoSize.CENTER;
				//this following line is probably unnecessary
				rbArray[i].group = rbGroupArray[ndiQuestionNumber];
				//rbArray[i].addEventListener(MouseEvent.CLICK,tracer);
				//since AS3 is stupid and you can't set radiobutton.selected to false, i will create a dummy radiobutton to select during transitions
			rbDummy.group=rbGroupArray[ndiQuestionNumber];
			rbDummy.selected=true;
			
			//
				addChild(rbArray[i]);
			}
			

		}

		/*		public function tracer(e:Event):void{
			trace(e.currentTarget.groupName);
		}*/


		public function ndi_NextClick(e: Event) {
			//only advance if somethin is selected
			//store the data
			for (k = 0; k < rbArray.length; k++) {
				if (rbArray[k].selected == true) {
					ndiScoreArray[ndiQuestionNumber] = k;
					//makes sure something is selected
					NextClickProceed();
				}

			}
		}

		public function NextClickProceed(): void {
			//reset value of rbArray while removing its children
			for (l = rbArray.length - 1; l >= 0; l--) {
				if (contains(rbArray[l])) {
					removeChild(rbArray[l]);
					rbArray.pop();
				}
			}

			//transition to new section when finished
			if (ndiQuestionNumber < ndiQuestionsArray.length - 1) {
				//generate new responses
				ndiQuestionNumber++;
				ndiQuestionsTextBox.text = ndiQuestionsArray[ndiQuestionNumber];
				resetRBs();
			} else {
				//remove all children
				removeChild(ndiQuestionsTextBox);
				removeChild(ndi_NextButton);
				removeChild(ndi_BackButton);

				for (i = 0; i < ndiScoreArray.length; i++) {
					ndiScoreTotal += ndiScoreArray[i];
				}
				cmsgmainloader.ndi = ndiScoreTotal / 50 * 100;
				dispatchEvent(new Event("sceneFF", true));
			}
		}

		public function ndi_BackClick(e: Event) {
			//reset value of rbArray while removing its children
			//dispatchEvent(new Event("resetRBEvent", true));	
			for (m = rbArray.length - 1; m >= 0; m--) {
				if (contains(rbArray[m])) {
					removeChild(rbArray[m]);
					rbArray.pop();
				}
			}
			//generate new responses
			ndiQuestionNumber--;
			//transition to prior section when pushing back button
			if (ndiQuestionNumber < 0) {
				removeChild(ndiQuestionsTextBox);
				dispatchEvent(new Event("sceneRev", true));
			} else {
				ndiQuestionsTextBox.text = ndiQuestionsArray[ndiQuestionNumber];
				resetRBs();
			}

		}
	}
}