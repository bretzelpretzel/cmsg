﻿package actions {
	import flash.display.MovieClip;
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	import flash.text.TextFormat;
	import flash.text.TextField;
	import actions.cmsgmainloader;
	import actions.upload;
	import flash.events.Event;

	public class scorereview extends MovieClip {

		var uploader: upload = new upload();
		var tf_next: TextFormat = new TextFormat();
		public function scorereview() {
			// constructor code
			institutionTextBox.text = cmsgmainloader.institution;
			uniqueIDTextBox.text = String(cmsgmainloader.uniqueID);
			ageTextBox.text = cmsgmainloader.age.toString();
			sexTextBox.text = cmsgmainloader.sex;
			handTextBox.text = cmsgmainloader.hand;
			mJOAScoreTextBox.text = cmsgmainloader.mjoa.toString();
			eat10ScoreTextBox.text = cmsgmainloader.eat10.toString();
			vhi10ScoreTextBox.text = cmsgmainloader.vhi10.toString();
			ndiScoreTextBox.text = cmsgmainloader.ndi.toString();
			sf36ScoreTextBox.text = "PF: " + int(cmsgmainloader.sf36_physfunc) + " RP: " + int(cmsgmainloader.sf36_roleffuncphys) + " RE: " + int(cmsgmainloader.sf36_rolefuncemotion) + " VT: " + int(cmsgmainloader.sf36_energy) + " MH: " + int(cmsgmainloader.sf36_emotionalwellbeing) + " SF: " + int(cmsgmainloader.sf36_socialfunc) + " BP: " + int(cmsgmainloader.sf36_pain) + " GH: " + int(cmsgmainloader.sf36_generalhealth) + " HC: " + int(cmsgmainloader.sf36_healthchange);;
			eq5dScoreTextBox.text = cmsgmainloader.eq5d_descriptive.toString() + " QALYs , VAS= " + cmsgmainloader.eq5d_vas.toString();
			fimsrightTextBox.text= cmsgmainloader.Challenge2Score[1] + " , " + cmsgmainloader.Challenge2Score[3];
			fimsleftTextBox.text= cmsgmainloader.Challenge2Score[0] + " , " + cmsgmainloader.Challenge2Score[2];
			upload_button.buttonMode = true;
			upload_button.setStyle("textFormat", tf_next);
			upload_button.addEventListener(MouseEvent.MOUSE_DOWN, checkCon);
			uploader.addEventListener("connectionChecked", advancer);
			tf_next.size = 32;
		}

		public function checkCon(e: MouseEvent): void {
			uploader.setInstitution(cmsgmainloader.institution);
			uploader.setUniqueID(cmsgmainloader.uniqueID);
			uploader.setAge(cmsgmainloader.age);
			uploader.setSex(cmsgmainloader.sex);
			uploader.setHand(cmsgmainloader.hand);
			uploader.setMJOA(cmsgmainloader.mjoa);
			uploader.setEAT10(cmsgmainloader.eat10);
			uploader.setVHI10(cmsgmainloader.vhi10);
			uploader.setNDI(cmsgmainloader.ndi);
			uploader.setSF36_PF(cmsgmainloader.sf36_physfunc);
			uploader.setSF36_RP(cmsgmainloader.sf36_roleffuncphys);
			uploader.setSF36_RE(cmsgmainloader.sf36_rolefuncemotion);
			uploader.setSF36_VT(cmsgmainloader.sf36_energy);
			uploader.setSF36_MH(cmsgmainloader.sf36_emotionalwellbeing);
			uploader.setSF36_SF(cmsgmainloader.sf36_socialfunc);
			uploader.setSF36_BP(cmsgmainloader.sf36_pain);
			uploader.setSF36_GH(cmsgmainloader.sf36_generalhealth);
			uploader.setSF36_HC(cmsgmainloader.sf36_healthchange);
			uploader.setEQ5D_descriptive(cmsgmainloader.eq5d_descriptive);
			uploader.setEQ5D_VAS(cmsgmainloader.eq5d_vas);
			uploader.setFMSLeft1(cmsgmainloader.Challenge2Score[0]);
			uploader.setFMSRight1(cmsgmainloader.Challenge2Score[1]);
			uploader.setFMSLeft2(cmsgmainloader.Challenge2Score[2]);
			uploader.setFMSRight2(cmsgmainloader.Challenge2Score[3]);
			uploader.checkConnection(e);
		}

		public function advancer(e: Event): void {
			trace("checking if OK to advance");
			if (uploader.getSuccessToggleValue() == "yes") {
				trace("moving to final scene");
				dispatchEvent(new Event("sceneFF", true));
			} else {
				trace("failure to upload");
			}
		}
	}
}