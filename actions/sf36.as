﻿package actions {

	import flash.display.MovieClip;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.text.TextFieldAutoSize;
	import fl.controls.RadioButton;
	import fl.controls.RadioButtonGroup;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import actions.cmsgmainloader;
	import flash.text.StyleSheet;
	import flash.text.Font;
	import flash.text.StyleSheet;



	public class sf36 extends MovieClip {
		//counting variables
		var i: int = new int();
		var j: int = new int();
		var k: int = new int();
		var l: int = new int();
		var m: int = new int();
		//other variables
		var sf36ScoreTotal: int = new int();
		//make text formats
		var tf_header: TextFormat = new TextFormat();
		var tf_prompt: TextFormat = new TextFormat();
		var tf_next: TextFormat = new TextFormat();
		var tf_radios: TextFormat = new TextFormat();
		//make text box
		var sf36QuestionsTextBox: TextField = new TextField();
		//start at first question
		var sf36QuestionNumber: int = new int();
		//make header array
		var sf36HeadersArray: Array = new Array();
		//make question array
		var sf36QuestionsArray: Array = new Array();
		//establish the responses array
		var sf36ResponsesArray: Array = new Array();
		//create radiobuttongroups
		var rbGroupArray: Array = new Array();
		//create radiobutton array
		var rbArray: Array = new Array();
		//create variable to store scores temporarily
		var sf36ScoreArray: Array = new Array();
		//create dummy radiobutton
		var rbDummy: RadioButton = new RadioButton();
		//for bolding text
		var css: StyleSheet = new StyleSheet();
		var fonts: Array = new Array();

		public function sf36() {
			// constructor code
			//dummy rb set up
			rbDummy.visible = false;
			//
			sf36ScoreTotal = 0;
			//
			fonts = (Font.enumerateFonts());
			for each(var fo in fonts) {
				trace(fo.fontName, ":", fo.fontStyle);
			}
			css.setStyle("bold", {
				fontFamily: "Times New Roman Bold"
			});
			tf_header.size = 36;
			tf_header.align = TextFormatAlign.CENTER;
			tf_prompt.size = 36;
			tf_prompt.align = TextFormatAlign.CENTER;
			tf_radios.size = 18;
			tf_next.size = 32;

			sf36_NextButton.setStyle("textFormat", tf_next);
			sf36_BackButton.setStyle("textFormat", tf_next);
			//set up header text box
			sf36HeadersTextBox.wordWrap = false;
			sf36HeadersTextBox.defaultTextFormat = tf_header;
			sf36HeadersTextBox.styleSheet = css;
			//sf36HeadersTextBox.autoSize=TextFieldAutoSize.CENTER;
			//set up text box
			sf36QuestionsTextBox.wordWrap = true;
			sf36QuestionsTextBox.multiline = true;
			sf36QuestionsTextBox.x = 0;
			sf36QuestionsTextBox.y = 330;
			sf36QuestionsTextBox.width = 640;
			sf36QuestionsTextBox.height = 220;
			sf36QuestionsTextBox.defaultTextFormat = tf_prompt;
			sf36QuestionsTextBox.styleSheet = css;
			//start at first question
			sf36QuestionNumber = 0;
			//establish the headers array
			sf36HeadersArray[0] = "Choose one option for each \n questionnaire item";
			sf36HeadersArray[1] = "Choose one option for each \n questionnaire item";
			sf36HeadersArray[2] = "The following items are about activities \n you might do during a typical day. \n Does <bold>your health now limit you</bold> in these activities? \n If so, how much?";
			sf36HeadersArray[3] = "The following items are about activities \n you might do during a typical day. \n Does <bold>your health now limit you</bold> in these activities? \n If so, how much?";
			sf36HeadersArray[4] = "The following items are about activities \n you might do during a typical day. \n Does <bold>your health now limit you</bold> in these activities? \n If so, how much?";
			sf36HeadersArray[5] = "The following items are about activities \n you might do during a typical day. \n Does <bold>your health now limit you</bold> in these activities? \n If so, how much?";
			sf36HeadersArray[6] = "The following items are about activities \n you might do during a typical day. \n Does <bold>your health now limit you</bold> in these activities? \n If so, how much?";
			sf36HeadersArray[7] = "The following items are about activities \n you might do during a typical day. \n Does <bold>your health now limit you</bold> in these activities? \n If so, how much?";
			sf36HeadersArray[8] = "The following items are about activities \n you might do during a typical day. \n Does <bold>your health now limit you</bold> in these activities? \n If so, how much?";
			sf36HeadersArray[9] = "The following items are about activities \n you might do during a typical day. \n Does <bold>your health now limit you</bold> in these activities? \n If so, how much?";
			sf36HeadersArray[10] = "The following items are about activities \n you might do during a typical day. \n Does <bold>your health now limit you</bold> in these activities? \n If so, how much?";
			sf36HeadersArray[11] = "The following items are about activities \n you might do during a typical day. \n Does <bold>your health now limit you</bold> in these activities? \n If so, how much?";
			sf36HeadersArray[12] = "During the <bold>past 4 weeks,</bold> have you had any of the following \n problems with your work or other regular \n daily activities <bold>as a result of your \n physical health?</bold>";
			sf36HeadersArray[13] = "During the <bold>past 4 weeks,</bold> have you had any of the following \n problems with your work or other regular \n daily activities <bold>as a result of your \n physical health?</bold>";
			sf36HeadersArray[14] = "During the <bold>past 4 weeks,</bold> have you had any of the following \n problems with your work or other regular \n daily activities <bold>as a result of your \n physical health?</bold>";
			sf36HeadersArray[15] = "During the <bold>past 4 weeks,</bold> have you had any of the following \n problems with your work or other regular \n daily activities <bold>as a result of your \n physical health?</bold>";
			sf36HeadersArray[16] = "During the <bold>past 4 weeks,</bold> have you had any of the following \n problems with your work or other regular \n daily activities <bold>as a result of any \n emotional problems</bold> (such as feeling \n depressed or anxious)?";
			sf36HeadersArray[17] = "During the <bold>past 4 weeks,</bold> have you had any of the following \n problems with your work or other regular \n daily activities <bold>as a result of any \n emotional problems</bold> (such as feeling \n depressed or anxious)?";
			sf36HeadersArray[18] = "During the <bold>past 4 weeks,</bold> have you had any of the following \n problems with your work or other regular \n daily activities <bold>as a result of any \n emotional problems</bold> (such as feeling \n depressed or anxious)?";
			sf36HeadersArray[19] = "Choose one option for each \n questionnaire item";
			sf36HeadersArray[20] = "Choose one option for each \n questionnaire item";
			sf36HeadersArray[21] = "Choose one option for each \n questionnaire item";
			sf36HeadersArray[22] = "How much of the time during the \n <bold>past 4 weeks...</bold>";
			sf36HeadersArray[23] = "How much of the time during the \n <bold>past 4 weeks...</bold>";
			sf36HeadersArray[24] = "How much of the time during the \n <bold>past 4 weeks...</bold>";
			sf36HeadersArray[25] = "How much of the time during the \n <bold>past 4 weeks...</bold>";
			sf36HeadersArray[26] = "How much of the time during the \n <bold>past 4 weeks...</bold>";
			sf36HeadersArray[27] = "How much of the time during the \n <bold>past 4 weeks...</bold>";
			sf36HeadersArray[28] = "How much of the time during the \n <bold>past 4 weeks...</bold>";
			sf36HeadersArray[29] = "How much of the time during the \n <bold>past 4 weeks...</bold>";
			sf36HeadersArray[30] = "How much of the time during the \n <bold>past 4 weeks...</bold>";
			sf36HeadersArray[31] = "Choose one option for each \n questionnaire item";
			sf36HeadersArray[32] = "How TRUE or FALSE is <bold>each</bold>  of the following statements for you";
			sf36HeadersArray[33] = "How TRUE or FALSE is <bold>each</bold>  of the following statements for you";
			sf36HeadersArray[34] = "How TRUE or FALSE is <bold>each</bold>  of the following statements for you";
			sf36HeadersArray[35] = "How TRUE or FALSE is <bold>each</bold>  of the following statements for you";
			//establish the questions array
			sf36QuestionsArray[0] = "In general, would you say your health is:";
			sf36QuestionsArray[1] = "<bold>Compared to one year ago,</bold> how would you rate your health in general <bold>now?</bold>";
			sf36QuestionsArray[2] = "<bold>Vigorous activities,</bold> such as running, lifting heavy objects, participating in strenuous sports";
			sf36QuestionsArray[3] = "<bold>Moderate activities,</bold> such as moving a table, pushing a vacuum cleaner, bowling, or playing golf";
			sf36QuestionsArray[4] = "Lifting or carrying groceries";
			sf36QuestionsArray[5] = "Climbing <bold>several</bold> flights of stairs";
			sf36QuestionsArray[6] = "Climbing <bold>one</bold> flight of stairs";
			sf36QuestionsArray[7] = "Bending, kneeling, or stooping";
			sf36QuestionsArray[8] = "Walking <bold>more than a mile</bold>";
			sf36QuestionsArray[9] = "Walking <bold>several blocks</bold>";
			sf36QuestionsArray[10] = "Walking <bold>one block</bold>";
			sf36QuestionsArray[11] = "Bathing or dressing yourself";
			sf36QuestionsArray[12] = "Cut down the <bold>amount of time</bold> you spent on work or other activities";
			sf36QuestionsArray[13] = "<bold>Accomplished less</bold> than you would like";
			sf36QuestionsArray[14] = "Were limited in the <bold>kind</bold> of work or other activities";
			sf36QuestionsArray[15] = "Had <bold>difficulty</bold> performing the work or other activities (for example, it took extra effort)";
			sf36QuestionsArray[16] = "Cut down the <bold>amount of time</bold> you spent on work or other activities";
			sf36QuestionsArray[17] = "<bold>Accomplished less</bold> than you would like";
			sf36QuestionsArray[18] = "Didn't do work or other activities as \n <bold>carefully</bold> as usual";
			sf36QuestionsArray[19] = "During the <bold>past 4 weeks, </bold>to what extent has your physical health or emotional problems interfered with your normal social activities with family, friends, neighbors, or groups?";
			sf36QuestionsArray[20] = "How much <bold>bodily</bold> pain have you had during the <bold>past 4 weeks?</bold>";
			sf36QuestionsArray[21] = "During the <bold>past 4 weeks,</bold> how much did <bold>pain</bold> interfere with your normal work \n(including both work outside the home \n and housework)?";
			sf36QuestionsArray[22] = "Did you feel full of pep?";
			sf36QuestionsArray[23] = "Have you been a very nervous person?";
			sf36QuestionsArray[24] = "Have you felt so down in the dumps that nothing could cheer you up?";
			sf36QuestionsArray[25] = "Have you felt calm and peaceful?";
			sf36QuestionsArray[26] = "Did you have a lot of energy?";
			sf36QuestionsArray[27] = "Have you felt downhearted and blue?";
			sf36QuestionsArray[28] = "Did you feel worn out?";
			sf36QuestionsArray[29] = "Have you been a happy person?";
			sf36QuestionsArray[30] = "Did you feel tired?";
			sf36QuestionsArray[31] = "During the <bold>past 4 weeks,</bold> how much of the time has <bold>your physical \n health or emotional problems</bold> interfered with your social activities (like \n visiting with friends, relatives, etc.)?";
			sf36QuestionsArray[32] = "I seem to get sick a litte easier than other people";
			sf36QuestionsArray[33] = "I am as healthy as anybody I know";
			sf36QuestionsArray[34] = "I expect my health to get worse";
			sf36QuestionsArray[35] = "My health is excellent";


			//establish the responses array
			sf36ResponsesArray[0] = ["1 - Excellent", "2 - Very good", "3 - Good", "4 - Fair", "5 - Poor"];
			sf36ResponsesArray[1] = ["Much better now than one  year ago", "Somewhat better now than one year ago", "About the same", "Somewhat worse now than one year ago", "Much worse now than one year ago"];
			sf36ResponsesArray[2] = ["1 - Yes, limited a lot", "2 - Yes, limited a little", "3 - No, not limited at all"];
			sf36ResponsesArray[3] = ["1 - Yes, limited a lot", "2 - Yes, limited a little", "3 - No, not limited at all"];
			sf36ResponsesArray[4] = ["1 - Yes, limited a lot", "2 - Yes, limited a little", "3 - No, not limited at all"];
			sf36ResponsesArray[5] = ["1 - Yes, limited a lot", "2 - Yes, limited a little", "3 - No, not limited at all"];
			sf36ResponsesArray[6] = ["1 - Yes, limited a lot", "2 - Yes, limited a little", "3 - No, not limited at all"];
			sf36ResponsesArray[7] = ["1 - Yes, limited a lot", "2 - Yes, limited a little", "3 - No, not limited at all"];
			sf36ResponsesArray[8] = ["1 - Yes, limited a lot", "2 - Yes, limited a little", "3 - No, not limited at all"];
			sf36ResponsesArray[9] = ["1 - Yes, limited a lot", "2 - Yes, limited a little", "3 - No, not limited at all"];
			sf36ResponsesArray[10] = ["1 - Yes, limited a lot", "2 - Yes, limited a little", "3 - No, not limited at all"];
			sf36ResponsesArray[11] = ["1 - Yes, limited a lot", "2 - Yes, limited a little", "3 - No, not limited at all"];
			sf36ResponsesArray[12] = ["1 - Yes", "2 - No"];
			sf36ResponsesArray[13] = ["1 - Yes", "2 - No"];
			sf36ResponsesArray[14] = ["1 - Yes", "2 - No"];
			sf36ResponsesArray[15] = ["1 - Yes", "2 - No"];
			sf36ResponsesArray[16] = ["1 - Yes", "2 - No"];
			sf36ResponsesArray[17] = ["1 - Yes", "2 - No"];
			sf36ResponsesArray[18] = ["1 - Yes", "2 - No"];
			sf36ResponsesArray[19] = ["1 - Not at all", "2 - Slightly", "3 - Moderately", "4 - Quite a bit", "5 - Extremely"];
			sf36ResponsesArray[20] = ["1 - None", "2 - Very Mild", "3 - Mild", "4 - Moderate", "5 - Severe", "6 - Very severe"];
			sf36ResponsesArray[21] = ["1 - Not at all", "2 - A little bit", "3 - Moderately", "4 - Quite a bit", "5 - Extremely"];
			sf36ResponsesArray[22] = ["1 - All of the time", "2 - Most of the time", "3 - A good bit of the time", "4 - Some of the time", "5 - A little of the time", "6 - None of the time"];
			sf36ResponsesArray[23] = ["1 - All of the time", "2 - Most of the time", "3 - A good bit of the time", "4 - Some of the time", "5 - A little of the time", "6 - None of the time"];
			sf36ResponsesArray[24] = ["1 - All of the time", "2 - Most of the time", "3 - A good bit of the time", "4 - Some of the time", "5 - A little of the time", "6 - None of the time"];
			sf36ResponsesArray[25] = ["1 - All of the time", "2 - Most of the time", "3 - A good bit of the time", "4 - Some of the time", "5 - A little of the time", "6 - None of the time"];
			sf36ResponsesArray[26] = ["1 - All of the time", "2 - Most of the time", "3 - A good bit of the time", "4 - Some of the time", "5 - A little of the time", "6 - None of the time"];
			sf36ResponsesArray[27] = ["1 - All of the time", "2 - Most of the time", "3 - A good bit of the time", "4 - Some of the time", "5 - A little of the time", "6 - None of the time"];
			sf36ResponsesArray[28] = ["1 - All of the time", "2 - Most of the time", "3 - A good bit of the time", "4 - Some of the time", "5 - A little of the time", "6 - None of the time"];
			sf36ResponsesArray[29] = ["1 - All of the time", "2 - Most of the time", "3 - A good bit of the time", "4 - Some of the time", "5 - A little of the time", "6 - None of the time"];
			sf36ResponsesArray[30] = ["1 - All of the time", "2 - Most of the time", "3 - A good bit of the time", "4 - Some of the time", "5 - A little of the time", "6 - None of the time"];
			sf36ResponsesArray[31] = ["1 - All of the time", "2 - Most of the time", "3 - Some of the time", "4 - A little of the time", "5 - None of the time"];
			sf36ResponsesArray[32] = ["1 - Definitely true", "2 - Mostly true", "3 - Don't know", "4 - Mostly false", "5 - Definitely false"];
			sf36ResponsesArray[33] = ["1 - Definitely true", "2 - Mostly true", "3 - Don't know", "4 - Mostly false", "5 - Definitely false"];
			sf36ResponsesArray[34] = ["1 - Definitely true", "2 - Mostly true", "3 - Don't know", "4 - Mostly false", "5 - Definitely false"];
			sf36ResponsesArray[35] = ["1 - Definitely true", "2 - Mostly true", "3 - Don't know", "4 - Mostly false", "5 - Definitely false"];

			////create radiobuttongroups
			//this section may be unnecessary
			for (j = 0; j < sf36QuestionsArray.length; j++) {
				var rbGroup: RadioButtonGroup = new RadioButtonGroup("options");
				rbGroupArray.push(rbGroup);
				resetRBs();
			}
			//set the starting values
			sf36HeadersTextBox.htmlText = sf36HeadersArray[sf36QuestionNumber];
			sf36QuestionsTextBox.htmlText = sf36QuestionsArray[sf36QuestionNumber];
			//			checkForBold("testing");
			//			checkForBold(sf36HeadersTextBox.text);
			addChild(sf36QuestionsTextBox);
			//advance the questions based on the button-presses
			sf36_NextButton.addEventListener(MouseEvent.CLICK, sf36_NextClick);
			sf36_BackButton.addEventListener(MouseEvent.CLICK, sf36_BackClick);

		}

		public function resetRBs(): void {
			for (i = 0; i < sf36ResponsesArray[sf36QuestionNumber].length; i++) {
				var rb: RadioButton = new RadioButton();

				rbArray.push(rb);
				rbArray[i].label = sf36ResponsesArray[sf36QuestionNumber][i];
				rbArray[i].x = 13;
				rbArray[i].y = 515 + 65 * i;
				rbArray[i].setStyle("textFormat", tf_radios);
				rbArray[i].width = 600;
				rbArray[i].height = 65;
				rbArray[i].value = 1;
				//	rbArray[i].textField.autoSize=TextFieldAutoSize.CENTER;
				//this following line is probably unnecessary
				rbArray[i].group = rbGroupArray[sf36QuestionNumber];
				//since AS3 is stupid and you can't set radiobutton.selected to false, i will create a dummy radiobutton to select during transitions
				rbDummy.group = rbGroupArray[sf36QuestionNumber];
				rbDummy.selected = true;
				addChild(rbArray[i]);
				//for testing purposes
				rbArray[i].addEventListener(MouseEvent.CLICK, tracer)
			}
		}
		public function tracer(e: Event): void {
			trace(e.currentTarget);
		}
		public function sf36_NextClick(e: Event) {
			//only advance if somethin is selected
			//store the data
			for (k = 0; k < rbArray.length; k++) {
				if (rbArray[k].selected == true) {
					//k+1 because 1st response is always 1 point
					sf36ScoreArray[sf36QuestionNumber] = k + 1;
					//makes sure something is selected
					NextClickProceed();
				}

			}
		}

		public function NextClickProceed(): void {
			//reset value of rbArray while removing its children
			for (l = rbArray.length - 1; l >= 0; l--) {
				if (contains(rbArray[l])) {
					removeChild(rbArray[l]);
					rbArray.pop();
				}
			}

			//transition to new section when finished
			if (sf36QuestionNumber < sf36QuestionsArray.length - 1) {
				//generate new responses
				sf36QuestionNumber++;
				sf36HeadersTextBox.text = sf36HeadersArray[sf36QuestionNumber];
				sf36QuestionsTextBox.text = sf36QuestionsArray[sf36QuestionNumber];
				resetRBs();
			} else {
				trace("test complete");
				//remove all children
				removeChild(sf36QuestionsTextBox);
				removeChild(sf36_NextButton);
				removeChild(sf36_BackButton);
				//Recode Scores as per the RAND SF-36
				for (var p: int = 0; p < sf36ScoreArray.length; p++) {
					if (p == 0 || p == 1 || p == 19 || p == 33 || p == 35) {
						sf36ScoreArray[p] = 100 - (sf36ScoreArray[p] - 1) * 25;
						trace("updated value");
					}
					if (p >= 2 && p <= 11) {
						sf36ScoreArray[p] = (sf36ScoreArray[p] - 1) * 50;
						trace("updated value");
					}
					if (p >= 12 && p <= 18) {
						sf36ScoreArray[p] = (sf36ScoreArray[p] - 1) * 100;
						trace("updated value");
					}
					if (p == 20 || p == 22 || p == 25 || p == 26 || p == 29) {
						sf36ScoreArray[p] = 100 - (sf36ScoreArray[p] - 1) * 20;
						trace("updated value");
					}
					if (p == 23 || p == 24 || p == 27 || p == 28 || p == 30) {
						sf36ScoreArray[p] = (sf36ScoreArray[p] - 1) * 20;
						trace("updated value");
					}
					if (p == 31 || p == 32 || p == 34) {
						sf36ScoreArray[p] = (sf36ScoreArray[p] - 1) * 25;
						trace("updated value");
						}

					}
					//now find the means for each grouping
					cmsgmainloader.sf36_physfunc = (sf36ScoreArray[2] + sf36ScoreArray[3] + sf36ScoreArray[4] + sf36ScoreArray[5] + sf36ScoreArray[6] + sf36ScoreArray[7] + sf36ScoreArray[8] + sf36ScoreArray[9] + sf36ScoreArray[10] + sf36ScoreArray[11]) / 10;
					cmsgmainloader.sf36_roleffuncphys = (sf36ScoreArray[12] + sf36ScoreArray[13] + sf36ScoreArray[14] + sf36ScoreArray[15]) / 4;
					cmsgmainloader.sf36_rolefuncemotion = (sf36ScoreArray[16] + sf36ScoreArray[17] + sf36ScoreArray[18]) / 3;
					cmsgmainloader.sf36_energy = (sf36ScoreArray[22] + sf36ScoreArray[26] + sf36ScoreArray[28] + sf36ScoreArray[30]) / 4;
					cmsgmainloader.sf36_emotionalwellbeing = (sf36ScoreArray[23] + sf36ScoreArray[24] + sf36ScoreArray[25] + sf36ScoreArray[27] + sf36ScoreArray[29]) / 5;
					cmsgmainloader.sf36_socialfunc = (sf36ScoreArray[19] + sf36ScoreArray[31]) / 2;
					cmsgmainloader.sf36_pain = (sf36ScoreArray[20] + sf36ScoreArray[21]) / 2;
					cmsgmainloader.sf36_generalhealth = (sf36ScoreArray[0] + sf36ScoreArray[32] + sf36ScoreArray[33] + sf36ScoreArray[34] + sf36ScoreArray[35]) / 5;
					cmsgmainloader.sf36_healthchange=sf36ScoreArray[1];
					dispatchEvent(new Event("sceneFF", true));
				}
			}

			public function sf36_BackClick(e: Event) {
				//reset value of rbArray while removing its children
				for (m = rbArray.length - 1; m >= 0; m--) {
					if (contains(rbArray[m])) {
						removeChild(rbArray[m]);
						rbArray.pop();
					}
				}
				//generate new responses
				sf36QuestionNumber--;
				//transition to prior section when pushing back button
				if (sf36QuestionNumber < 0) {
					removeChild(sf36QuestionsTextBox);
					dispatchEvent(new Event("sceneRev", true));
				} else {
					trace("this is question " + sf36QuestionNumber + " and there are " + sf36QuestionsArray.length + " questions");
					sf36QuestionsTextBox.text = sf36QuestionsArray[sf36QuestionNumber];
					resetRBs();
				}

			}
		}
	}