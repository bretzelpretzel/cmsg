﻿package actions {

	import flash.display.MovieClip;
	import flash.events.*;
	import flash.net.*;
	import flash.text.*;

	public class upload extends MovieClip {

		var institution: String = new String();
		var uniqueID: int = new int();
		var age: Number = new Number();
		var sex: String = new String();
		var hand: String = new String();
		var mjoa: int = new int();
		var eat10: int = new int();
		var vhi10: int = new int();
		var ndi: int = new int();
		var SF36_PF: Number = new Number();
		var SF36_RP: Number = new Number();
		var SF36_RE: Number = new Number();
		var SF36_VT: Number = new Number();
		var SF36_MH: Number = new Number();
		var SF36_SF: Number = new Number();
		var SF36_BP: Number = new Number();
		var SF36_GH: Number = new Number();
		var SF36_HC: Number = new Number();
		var eq5d_descriptive: Number = new Number();
		var eq5d_vas: int = new int();
		var fmsRight1: int = new int();
		var fmsRight2: int = new int();
		var fmsLeft1: int = new int();
		var fmsLeft2: int = new int();


		//for advancement
		var successToggle: String = new String();

		public function upload() {
			// constructor code
			trace("upload constructor running");
			successToggle = "no";
		}

		public function setInstitution(s: String): void {
			institution = s;
		}
		public function getInstitution(): String {
			return institution;
		}
		public function setUniqueID(i: int): void {
			uniqueID = i;
		}
		public function getUniqueID(): int {
			return uniqueID;
		}
		public function setAge(i: Number): void {
			age = i;
		}
		public function getAge(): Number {
			return age;
		}
		public function setSex(s: String): void {
			sex = s;
		}
		public function getSex(): String {
			return sex;
		}

		public function setHand(s: String): void {
			hand = s;
		}
		public function getHand(): String {
			return hand;
		}
		public function setMJOA(i: int): void {
			mjoa = i;
		}
		public function getMJOA(): int {
			return mjoa;
		}
		public function setEAT10(i: int): void {
			eat10 = i;
		}
		public function getEAT10(): int {
			return eat10;
		}
		public function setVHI10(i: int): void {
			vhi10 = i;
		}
		public function getVHI10(): int {
			return vhi10;
		}
		public function setNDI(i: int): void {
			ndi = i;
		}
		public function getNDI(): int {
			return ndi;
		}
		//physical functioning
		public function setSF36_PF(i: Number): void {
			SF36_PF = i;
		}
		public function getSF36_PF(): Number {
			return SF36_PF;
		}
		//Role Functioning/ Physical
		public function setSF36_RP(i: Number): void {
			SF36_RP = i;
		}
		public function getSF36_RP(): Number {
			return SF36_RP;
		}
		//Role Functioning / Emotional
		public function setSF36_RE(i: Number): void {
			SF36_RE = i;
		}
		public function getSF36_RE(): Number {
			return SF36_RE;
		}
		//vitality
		public function setSF36_VT(i: Number): void {
			SF36_VT = i;
		}
		public function getSF36_VT(): Number {
			return SF36_VT;
		}
		//mental health
		public function setSF36_MH(i: Number): void {
			SF36_MH = i;
		}
		public function getSF36_MH(): Number {
			return SF36_MH;
		}
		//social functioning
		public function setSF36_SF(i: Number): void {
			SF36_SF = i;
		}
		public function getSF36_SF(): Number {
			return SF36_SF;
		}
		//Bodily Pain
		public function setSF36_BP(i: Number): void {
			SF36_BP = i;
		}
		public function getSF36_BP(): Number {
			return SF36_BP;
		}
		//general health
		public function setSF36_GH(i: Number): void {
			SF36_GH = i;
		}
		public function getSF36_GH(): Number {
			return SF36_GH;
		}
		//health change
		public function setSF36_HC(i: Number): void {
			SF36_HC = i;
		}
		public function getSF36_HC(): Number {
			return SF36_HC;
		}
		public function setEQ5D_descriptive(i: Number): void {
			eq5d_descriptive = i;
		}
		public function getEQ5D_descriptive(): Number {
			return eq5d_descriptive;
		}
		public function setEQ5D_VAS(i: int): void {
			eq5d_vas = i;
		}
		public function getEQ5D_VAS(): int {
			return eq5d_vas;
		}
		public function setFMSRight1(i: int): void {
			fmsRight1 = i;
		}
		public function getFMSRight1(): int {
			return fmsRight1;
		}
		public function setFMSRight2(i: int): void {
			fmsRight2 = i;
		}
		public function getFMSRight2(): int {
			return fmsRight2;
		}
		public function setFMSLeft1(i: int): void {
			fmsLeft1 = i;
		}
		public function getFMSLeft1(): int {
			return fmsLeft1;
		}
		public function setFMSLeft2(i: int): void {
			fmsLeft2 = i;
		}
		public function getFMSLeft2(): int {
			return fmsLeft2;
		}
		public function checkConnection(e: MouseEvent): void {
			var phpVars2: URLVariables = new URLVariables();
			//add subject php url
			var phpFileRequest2: URLRequest = new URLRequest("https://1drv.ms/u/s!Ag89u0eLEJNRgsJFMOA4_HBOzCrsXQ");
			phpFileRequest2.method = URLRequestMethod.POST;
			phpFileRequest2.data = phpVars2;
			var phpLoader2: URLLoader = new URLLoader();
			phpLoader2.dataFormat = URLLoaderDataFormat.VARIABLES;
			phpLoader2.addEventListener(Event.COMPLETE, showResult2);
			phpVars2.systemCall = "upload";
			//add in variables here
			phpVars2.institution = institution;
			phpVars2.uniqueID = uniqueID;
			//phpVars2.timestamp = 1234;
			phpVars2.age = age;
			phpVars2.sex = sex;
			phpVars2.mjoa = mjoa;
			phpVars2.eat10 = eat10;
			phpVars2.vhi10 = vhi10;
			phpVars2.ndi = ndi;
			phpVars2.sf36_PF = SF36_PF;
			phpVars2.sf36_RP = SF36_RP;
			phpVars2.sf36_RE = SF36_RE;
			phpVars2.sf36_VT = SF36_VT;
			phpVars2.sf36_MH = SF36_MH;
			phpVars2.sf36_SF = SF36_SF;
			phpVars2.sf36_BP = SF36_BP;
			phpVars2.sf36_GH = SF36_GH;
			phpVars2.sf36_HC = SF36_HC;
			phpVars2.eq5d_descriptive = eq5d_descriptive;
			phpVars2.eq5d_vas = eq5d_vas;
			phpVars2.fmsRight1 = fmsRight1;
			phpVars2.fmsRight2 = fmsRight2;
			phpVars2.fmsLeft1 = fmsLeft1;
			phpVars2.fmsLeft2 = fmsLeft2;


			phpLoader2.load(phpFileRequest2);

		}
		public function showResult2(event: Event): void {
			trace('upload attempted');
			if (event.target.data.systemResult == "success") {
				trace("upload successful");
				successToggle = "yes";
				dispatchEvent(new Event("connectionChecked"));
			} else {
				trace("failed upload");
			}
		}
		public function getSuccessToggleValue(): String {
			return successToggle;
		}

	}

}