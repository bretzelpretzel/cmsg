﻿package actions {

	import flash.display.MovieClip;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import fl.controls.RadioButton;
	import fl.controls.RadioButtonGroup;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import actions.cmsgmainloader;



	public class vhi10 extends MovieClip {
		//counting variables
		var i: int = new int();
		var j: int = new int();
		var k: int = new int();
		var l: int = new int();
		var m: int = new int();
		//other variables
		var vhi10ScoreTotal: int = new int();
		//make text formats
		var tf_prompt: TextFormat = new TextFormat();
		var tf_next: TextFormat = new TextFormat();
		var tf_radios: TextFormat = new TextFormat();
		//make text box
		var vhi10QuestionsTextBox: TextField = new TextField();
		//start at first question
		var vhi10QuestionNumber: int = new int();
		//make question array
		var vhi10QuestionsArray: Array = new Array();
		//establish the responses array
		var vhi10ResponsesArray: Array = new Array();
		//create radiobuttongroups
		var rbGroupArray: Array = new Array();
		//create radiobutton array
		var rbArray: Array = new Array();
		//create variable to store scores temporarily
		var vhi10ScoreArray: Array = new Array();
		//create dummy radiobutton
		var rbDummy:RadioButton = new RadioButton();

		public function vhi10() {
			// constructor code
			//dummy rb set up
			rbDummy.visible=false;
			//
			vhi10ScoreTotal = 0;
			tf_prompt.size = 36;
			tf_prompt.align = TextFormatAlign.CENTER;
			tf_radios.size = 14;
			tf_next.size = 32;
			vhi10_NextButton.setStyle("textFormat", tf_next);
			vhi10_BackButton.setStyle("textFormat", tf_next);
			//set up text box
			vhi10QuestionsTextBox.wordWrap = true;
			vhi10QuestionsTextBox.x = 0;
			vhi10QuestionsTextBox.y = 62;
			vhi10QuestionsTextBox.width = 640;
			vhi10QuestionsTextBox.height = 140;
			vhi10QuestionsTextBox.defaultTextFormat = tf_prompt;
			//start at first question
			vhi10QuestionNumber = 0;
			//establish the questions array
			vhi10QuestionsArray[0] = "My voice makes it difficult for people to hear me.";
			vhi10QuestionsArray[1] = "I run out of air when I talk.";
			vhi10QuestionsArray[2] = "People have difficulty understanding me in a noisy room.";
			vhi10QuestionsArray[3] = "The sound of my voice varies throughout the day.";
			vhi10QuestionsArray[4] = "My family has difficulty hearing me when I call them throughout the house.";
			vhi10QuestionsArray[5] = "I use the phone less often than I would like to.";
			vhi10QuestionsArray[6] = "I'm tense when talking to others because of my voice.";
			vhi10QuestionsArray[7] = "I tend to avoid groups of people because of my voice.";
			vhi10QuestionsArray[8] = "People seem irritated with my voice.";
			vhi10QuestionsArray[9] = "People ask, \"What's wrong with your voice?\"";


			//establish the responses array
			vhi10ResponsesArray[0] = ["0 - never", "1 - almost never", "2 - sometimes", "3 - almost always", "4 - always"];
			vhi10ResponsesArray[1] = ["0 - never", "1 - almost never", "2 - sometimes", "3 - almost always", "4 - always"];
			vhi10ResponsesArray[2] = ["0 - never", "1 - almost never", "2 - sometimes", "3 - almost always", "4 - always"];
			vhi10ResponsesArray[3] = ["0 - never", "1 - almost never", "2 - sometimes", "3 - almost always", "4 - always"];
			vhi10ResponsesArray[4] = ["0 - never", "1 - almost never", "2 - sometimes", "3 - almost always", "4 - always"];
			vhi10ResponsesArray[5] = ["0 - never", "1 - almost never", "2 - sometimes", "3 - almost always", "4 - always"];
			vhi10ResponsesArray[6] = ["0 - never", "1 - almost never", "2 - sometimes", "3 - almost always", "4 - always"];
			vhi10ResponsesArray[7] = ["0 - never", "1 - almost never", "2 - sometimes", "3 - almost always", "4 - always"];
			vhi10ResponsesArray[8] = ["0 - never", "1 - almost never", "2 - sometimes", "3 - almost always", "4 - always"];
			vhi10ResponsesArray[9] = ["0 - never", "1 - almost never", "2 - sometimes", "3 - almost always", "4 - always"];

			////create radiobuttongroups
			//this section may be unnecessary
			for (j = 0; j < vhi10QuestionsArray.length; j++) {
				var rbGroup: RadioButtonGroup = new RadioButtonGroup("options");
				rbGroupArray.push(rbGroup);
				resetRBs();
			}
			//set the starting values

			vhi10QuestionsTextBox.text = vhi10QuestionsArray[vhi10QuestionNumber];
			addChild(vhi10QuestionsTextBox);
			//advance the questions based on the button-presses
			vhi10_NextButton.addEventListener(MouseEvent.CLICK, vhi10_NextClick);
			vhi10_BackButton.addEventListener(MouseEvent.CLICK, vhi10_BackClick);

		}

		public function resetRBs(): void {
			for (i = 0; i < vhi10ResponsesArray[vhi10QuestionNumber].length; i++) {
				var rb: RadioButton = new RadioButton();

				rbArray.push(rb);
				rbArray[i].label = vhi10ResponsesArray[vhi10QuestionNumber][i];
				rbArray[i].x = 13;
				rbArray[i].y = 230 + 65 * i;
				rbArray[i].setStyle("textFormat", tf_radios);
				rbArray[i].width = 600;
				rbArray[i].height = 65;
				rbArray[i].value = 1;
				//	rbArray[i].textField.autoSize=TextFieldAutoSize.CENTER;
				//this following line is probably unnecessary
				rbArray[i].group = rbGroupArray[vhi10QuestionNumber];
				//since AS3 is stupid and you can't set radiobutton.selected to false, i will create a dummy radiobutton to select during transitions
				rbDummy.group = rbGroupArray[vhi10QuestionNumber];
				rbDummy.selected = true;
				addChild(rbArray[i]);
			}
		}

		public function vhi10_NextClick(e: Event) {
			//only advance if somethin is selected
			//store the data
			for (k = 0; k < rbArray.length; k++) {
				if (rbArray[k].selected == true) {
					vhi10ScoreArray[vhi10QuestionNumber] = k;
					//makes sure something is selected
					NextClickProceed();
				}

			}
		}

		public function NextClickProceed(): void {
			//reset value of rbArray while removing its children
			for (l = rbArray.length - 1; l >= 0; l--) {
				if (contains(rbArray[l])) {
					removeChild(rbArray[l]);
					rbArray.pop();
				}
			}

			//transition to new section when finished
			if (vhi10QuestionNumber < vhi10QuestionsArray.length - 1) {
				//generate new responses
				vhi10QuestionNumber++;
				vhi10QuestionsTextBox.text = vhi10QuestionsArray[vhi10QuestionNumber];
				resetRBs();
			} else {
				trace("test complete");
				//remove all children
				removeChild(vhi10QuestionsTextBox);
				removeChild(vhi10_NextButton);
				removeChild(vhi10_BackButton);

				for (i = 0; i < vhi10ScoreArray.length; i++) {
					vhi10ScoreTotal += vhi10ScoreArray[i];
				}
				cmsgmainloader.vhi10 = vhi10ScoreTotal;
				dispatchEvent(new Event("sceneFF", true));
			}
		}

		public function vhi10_BackClick(e: Event) {
			//reset value of rbArray while removing its children
			for (m = rbArray.length - 1; m >= 0; m--) {
				if (contains(rbArray[m])) {
					removeChild(rbArray[m]);
					rbArray.pop();
				}
			}
			//generate new responses
			vhi10QuestionNumber--;
			//transition to prior section when pushing back button
			if (vhi10QuestionNumber < 0) {
				removeChild(vhi10QuestionsTextBox);
				dispatchEvent(new Event("sceneRev", true));
			} else {
				trace("this is question " + vhi10QuestionNumber + " and there are " + vhi10QuestionsArray.length + " questions");
				vhi10QuestionsTextBox.text = vhi10QuestionsArray[vhi10QuestionNumber];
				resetRBs();
			}

		}
	}

}