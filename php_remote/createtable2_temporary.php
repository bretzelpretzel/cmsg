<?php
/*
connect to our database
*/
require_once "connect.php";
/*
create the mysql query
*/
$sql = "CREATE TABLE subjects (                  
        id int(11) NOT NULL auto_increment,
       institution varchar(255),
       uniqueid BIGINT,
        timestamp DateTime NOT NULL,
      age FLOAT,
        sex varchar(7),
        hand varchar(6),
        mjoa INTEGER,
         eat10 INTEGER,
        vhi10 INTEGER,
         ndi INTEGER,
         sf36_PF FLOAT,
         sf36_RP FLOAT,
         sf36_RE FLOAT,
         sf36_VT FLOAT,
         sf36_MH FLOAT,
         sf36_SF FLOAT,
         sf36_BP FLOAT,
         sf36_GH FLOAT,
         sf36_HC FLOAT,
        eq5d_descriptive FLOAT,
        eq5d_vas INTEGER,
        fmsRight1 INTEGER,
        fmsRight2 INTEGER,
        fmsLeft1 INTEGER,
        fmsLeft2 INTEGER,
        PRIMARY KEY (id)
        )";
$query = mysqli_query($link,$sql);
if ($query === TRUE) {
echo 'You have successfully created your table';
}
mysqli_close($link);
?>