﻿package qalydata {

	import actions.cmsgmainloader;
	import qalydata.USA;
	import qalydata.string;


	public class eq5dq_calculator {
		var eq5dqStringArray: Array = new Array();
		var qalyArrayUSA: Array = new Array();


		public function eq5dq_calculator() {
			// constructor code

			//assuming the person is from USA
			eq5dqStringArray = qalydata.string.qalyString;
			qalyArrayUSA = qalydata.USA.qalyArray;


		}
		public function calculateQALYs(j: int) {

			for (var i: int = 0; i < eq5dqStringArray.length; i++) {
				if (eq5dqStringArray[i] == j) {
					cmsgmainloader.eq5d_descriptive = qalyArrayUSA[i];
					trace("setting cmsgmainloader.eq5d_descriptive to " + qalyArrayUSA[i]);
				}

			}
		}
	}
}